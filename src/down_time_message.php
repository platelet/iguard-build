<?php

?>

<html>
	
	<head>
		
		<!-- meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- title -->
		<title>iGuard</title>
		
		<!-- stylesheets -->
		<link href="css/main.css" rel="stylesheet">
		<link href="css/flaticon.css" rel="stylesheet">
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="bootstrap/css/prettify.css" rel="stylesheet">
		<!-- flat-ui -->
		<link href="flat/css/flat-ui.css" rel="stylesheet">
		
		<!-- scripts -->
		<script src="js/jquery.min.js" type="text/javascript"></script>
		<script src="js/main.js" type="text/javascript"></script>
		<script src="js/date.js" type="text/javascript"></script>
		
		<style>
			
			h1.downtimenotice
			{
				text-align: center;
				font-size: 40px;
				margin-top: 250px;
				color: #ecf0f1;
			}
			
		</style>
		
	</head>
	
	<body>
		
		<div id="pageWrapper">
			
			<div class="no-sel" id="homeHeaderWrapper">
				<h1 class="no-sel">iGuard</h1>
			</div>
			
			<div id="contentWrapper">
				<h1 class="downtimenotice">iGuard has been moved to <a href="http://iguard.kiwi">iguard.kiwi</a></h1>
			</div>
			
		</div>
		
		<script src="flat/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="flat/js/jquery.ui.touch-punch.min.js"></script>
    <script src="flat/js/bootstrap.min.js"></script>
    <script src="flat/js/bootstrap-select.js"></script>
    <script src="flat/js/bootstrap-switch.js"></script>
    <script src="flat/js/flatui-checkbox.js"></script>
    <script src="flat/js/flatui-radio.js"></script>
    <script src="flat/js/jquery.tagsinput.js"></script>
    <script src="flat/js/jquery.placeholder.js"></script>
    <script src="flat/js/typeahead.js"></script>
    <script src="bootstrap/js/google-code-prettify/prettify.js"></script>
    <script src="flat/js/application.js"></script>
		
	</body>
	
</html>