/* RUN TIME CODE */
$(function() {

	window.logs = []; //globals log array

	setTimestamps(); //format times into time gone
	setUpTimestampUI();

	$('.log-log-time').each(function() {
		$(this).text(Date.parseExact($(this).text(), 'yyyy-MM-dd HH:mm:ss').toString('HH:mm   dd/MM/yyyy'));
	});

	$('select.nrm').selectpicker({style: 'btn-primary btn-block btn-lg', menuStyle: 'dropdown-inverse'}); //select picker

	//bind tab changing buttons
	$('.tab-btn').click(function() {
		if ($(this).attr('data-tab') == '1') {setCurrentTime();}  //if its to a log entry, reset the time
		$('.tab').addClass('hidden');  //hide all tabs
		$('.tab-btn').removeClass('selected');  //reset all tab buttons
		$(this).addClass('selected');  //select the new tab button
		$('.tab[data-tab="'+$(this).attr('data-tab')+'"]').removeClass('hidden');  //show the new tab
		$('p.current-tab').text($('.tab[data-tab="'+$(this).attr('data-tab')+'"]').attr('data-name'));  //update the UI
	});

	//bind switch events
	$('.switch').find('label').click(function() {checkFilterState();});
	$('.switch').click(function() {checkFilterState();});

	$('.log-entry-wrapper').each(function() {
		var l = {};
		l.logType = $(this).find('.log-log-type').text();
		l.timestamp = $(this).find('.log-log-time').text();
		l.guardName = $(this).find('.log-guard-name').text();
		l.category = $(this).find('.log-category').text();
		l.logComment = $(this).find('.log-log-comment').text();
		l.locationName = $(this).find('.log-location-name').text();
		window.logs.push(l);
	});

  //bind enter key in message box
	$('input[name="new-msg"]').keypress(function(e) {
		if (e.keyCode == 13) {sendMessage();}
	});


	$('input[id="alert-from"]').val(Date.now().addDays(-2).toString('HH:mm dd/MM/yyyy'));
	$('input[id="alert-to"]').val(Date.now().addDays(2).toString('HH:mm dd/MM/yyyy'));

	if ($('.log-entry-wrapper').length == 0) {$('.log-error').show();}

	//autosize textarea
	$('textarea').textareaAutoSize();

	checkStorageForIrContent();
	setInterval(saveIrContentToStorage, 10000);

	aZ();
	$('body').mousemove(cZ).click(cZ).keydown(cZ);  //reset the timeout for events
});

function saveIrContentToStorage() {
	localStorage.setItem('ir-form', JSON.stringify(collectLEData(0)));
}

function checkStorageForIrContent() {
	if (localStorage.getItem('ir-form') !== null) {
		var formData = JSON.parse(localStorage.getItem('ir-form'));
		$('textarea[name="c"]').val(formData.formData[1].value.replace(/(\<br\s\/\>)/g, '\n'));
	}
}

function pad(num) {
	if (String(num).length === 1) {
		return '0' + String(num);
	}
	return String(num);
}

function clearStorage() {
	localStorage.removeItem('ir-form');
}

//inactivity functions
function cZ() {
	clearInterval(z);  //clear interval
	aZ();  //reset
}
function aZ() {
	window.z = setInterval(function() {window.location.reload(); }, 600000);  //create 10 min timeout
}

//alert filter
function applyAlertFilter() {
	//reset the filtered array
	window.filterArray = [];
	var error = false;
	$('.filter-error').hide();
	//location
	var location = selectedOption('alert-location');
	//category
	var category = selectedOption('alert-cat');
	//logtype
	var logType = selectedOption('alert-lt');
	//guard
	var guard = selectedOption('alert-guard');

	//from
	var from = Date.parseExact($('input[id="alert-from"]').val(), 'HH:mm dd/MM/yyyy');
	var to = Date.parseExact($('input[id="alert-to"]').val(), 'HH:mm dd/MM/yyyy');
	if (from != null) {
	} else {
		$('#a-f-error').show();
		error = true;
	}
	//to
	if (to != null) {
	} else {
		$('#a-t-error').show();
		error = true;
	}
	//check they are not in to-from order
	if (!error) {
		if (from > to) {
			$('#a-d-error').show();
			error = false;
		}
	}

	//iterate data and populate html
	if (!error) {
		for (i in window.logs) {
			var l = window.logs[i];
			var fit = true;
			if (location.indexOf(l.locationName) == -1) {
				fit = false;
			}
			if (category.indexOf(l.category) == -1) {
				fit = false;
			}
			if (logType.indexOf(l.logType) == -1) {
				fit = false;
			}
			if (guard.indexOf(l.guardName) == -1) {
				fit = false;
			}
			if (Date.parse(l.timestamp) < from || Date.parse(l.timestamp) > to) {
				fit = false;
			}

			if (fit) {
				window.filterArray.push(l);
			}
		}
	}

	//redraw
	if (window.filterArray.length == 0) {
		$('.alerts-viewer-wrap').html('').append($('<p class="message-error">').text('No logs fit that filter'));
		$('#alert-hr').hide();
		$('#alert-btn').hide();
	} else {
		var h = logHTML(filterArray);
		$('.alerts-viewer-wrap').html(h);
		$('#alert-hr').show();
		$('#alert-btn').show();
	}
}

//generate HTML for a log entry
function logHTML(d) {
	var h = '';
	for (i in d) {
		var l = d[i];
    var cl = (l.logType === 'Log Entry') ? 'log-type-title' : 'ir-type-title';
		h += '<div class="log-entry-wrapper"><span class="log-location-name">'+l.locationName+'</span><span class="log-info-sep"> | </span><span class="log-log-type '+cl+'">'+l.logType+'</span><span class="log-info-sep"> | </span><span class="log-log-time">'+Date.parseExact(l.timestamp, 'yyyy-MM-dd HH:mm:ss').toString('HH:mm   dd/MM/yyyy')+'</span><br><span class="log-guard-name">'+l.guardName+'</span><span class="log-info-sep"> | </span><span class="log-category">'+l.category+'</span><span class="log-info-sep"> | </span><span class="log-log-comment">'+l.logComment+'</span></div>';
	}
	return h;
}

//get the selected option
function selectedOption(aId) {
	var a = $('#'+aId).parent().find('.btn-group.select').find('ul').find('.selected');  //array of selected elements
	var rs = '';  //result
	for (var i=0;i < a.length;i++) {
		rs += $(a[i]).find('span').text() + ':';
	}
	return rs.substr(0, rs.length-1).split(':');
}


function outputExcel() {
	if (window.filterArray.length != 0) {
		$.post('/exportLogExcel', { data : window.filterArray, timestamp : Date.now() }, function(data) {
			if (data) {
				var a = window.open('http://'+window.location.host+'/downloadExcel/'+data, '_blank');
				a.focus();
			}
		});
	}
}


function checkFilterState() {
	if ($('.switch').find('.switch-on').length == 1) {
		$('.log-controls-wrap').show().animate({ height : '420px' }, 500);
	} else {
		$('.log-controls-wrap').animate({ height : '0px' }, 500);
	}
}


function setTimestamps() {
	//change all the dates to a reformated time
	$('.date').each(function() {
		$(this).text(timeDifference($(this).attr('data-timestamp')));
	})
}


function getMessages() {
	$.post('/getMessages', {location:$('.location-window').attr('data-id')}, function(data) {
		data = $.parseJSON(data);
		var h = '';
		for (i in data.messages) {
			var m = data.messages[i];
			if (m.username == data.user) {var d='to';} else {var d='from';}
			h += msgDiv(m.username,m.message,m.sentAt,d);
		}
		$('#msg-wrap-div').html(h);
		setTimestamps();
	});
}


function msgDiv(u,m,t,d) {
	return '<div class="message-wrapper '+d+'"><p class="msg-user">'+u+'</p><p class="message">'+m+'</p><p class="date" data-timestamp="'+t+'"></p></div>';
}


function sendMessage() {
	if ($('input[name="new-msg"]').val().length != 0) {
		$('input[name="new-msg"]').attr('disabled', '');
		$.post('/newMessage', { message : $('input[name="new-msg"]').val(), location : $('.location-window').attr('data-id') }, function(data) {
			if (data != false) {getMessages();}
			$('input[name="new-msg"]').removeAttr('disabled').val('');
		});
	}
}

//put time values into log entry drop downs
function setUpTimestampUI() {
  //iterate
	for (var i=0;i<60;i++) {
		var j = td(i); //format
		$('[data-dt-part="minute"]').append($('<option>').val(j).text(j)); //append
	}
	for (var i=0;i<24;i++) {
		var j = td(i);
		$('[data-dt-part="hour"]').append($('<option>').val(j).text(j));
	}
	for (var i=1;i<32;i++) {
	  var j = td(i);
		$('[data-dt-part="day"]').append($('<option>').val(j).text(j));
	}
	for (var i=1;i<13;i++) {
	  var j = td(i);
		$('[data-dt-part="month"]').append($('<option>').val(j).text(j));
	}
	for (var i=new Date().getFullYear();i<new Date().getFullYear()+1;i++) {
		$('[data-dt-part="year"]').append($('<option>').val(i).text(i));
	}
	setCurrentTime();
}

//set the current time in the log entry dropdowns
function setCurrentTime() {
	var now = Date.now(); //current time
	$('[data-dt-part="hour"]').val(td(now.getHours()));
	$('[data-dt-part="minute"]').val(td(now.getMinutes()));
	$('[data-dt-part="day"]').val(td(now.getDate()));
	$('[data-dt-part="month"]').val(td(now.getMonth()+1));
	$('[data-dt-part="year"]').val(String(new Date().getFullYear()));
	$('select[data-dt-part]').selectpicker({style: 'btn-primary btn-lg', menuStyle: 'dropdown-inverse'});
	$('select[data-dt-part]').selectpicker('refresh');
}

//helper function for creating 2 digit numbers
// '2' --> '02'
function td(a) {
  return (String(a).length == 1) ? "0"+String(a) : a;
}

function timeDifference(timestamp) {
	var msgTime = Date.parse(timestamp);
	var difference = Date.now() - msgTime;
	var dif_in_secs = Math.floor(difference / 1000);
	var dif_in_mins = Math.floor(difference / 60000);
	var dif_in_hours = Math.floor(difference / 3600000);
	if (dif_in_secs <= 60) {
		return dif_in_secs + ' seconds ago';
	} else if (dif_in_mins <= 60) {
		return dif_in_mins + ' minutes ago';
	} else if (dif_in_hours <= 24) {
		return dif_in_hours + ' hours ago';
	} else {
		return Date.parse(timestamp).toString('HH:mm   d-MMM');
	}
}

function createLog() {
	$('#new-log-form-wrap').fadeOut(10);
	$('#loading-tasks').show().children().hide();
	$('p[data-info="0"]').show();
	$.post('/createLog', collectLEData(0), function(data) {
		console.log(data);
		$('p[data-info="0"]').hide();
		$('p[data-info="2"]').show();
		$('[name="c"]').val('');
		setTimeout(function() {
			collectLogs();
			clearStorage();
			$('#loading-tasks').hide().children().hide();
			$('#new-log-form-wrap').fadeIn(10);
		}, 500);
	});
}
function createIR() {
	$('#new-log-form-wrap').fadeOut(300);
	$('#loading-tasks').show().children().hide();
	$('p[data-info="1"]').show();
	$.post('/createLog', collectLEData(1), function(data) {
		console.log(data);
		$('p[data-info="1"]').hide();
		$('p[data-info="3"]').show();
		$('[name="c"]').val('');
		setTimeout(function() {
			collectLogs();
			clearStorage();
			$('#loading-tasks').hide().children().hide();
			$('#new-log-form-wrap').fadeIn(10);
		}, 500);
	});
}
function collectLEData(n) {
	window.dt = $('select[data-dt-part]');
	var ts = { hour : parseInt($(dt[0]).find(':selected').text()), minute : parseInt($(dt[1]).find(':selected').text()), day : parseInt($(dt[2]).find(':selected').text()), month : parseInt($(dt[3]).find(':selected').text()), year : new Date().getFullYear() };
	var fd = $('[name="new-log"]').serializeArray();
	for (i in fd) {
		if (fd[i].name === 'c') {
			fd[i].value = fd[i].value.replace(/\r?\n/g, '<br />')
		}
	}
	return { timestamp : ts, formData : fd, type : n, location : $('.location-window').attr('data-id') };
}
function collectLogs() {
	$.post('/getLogs', { location : $('.location-window').attr('data-id') }, function(data) {
		data = $.parseJSON(data);
		var h = '<h3 class="message-error">no logs found</h3>';
		for (i in data) {
			var l = data[i];
			if (l.logType == 0) {
				var t = 'Log Entry';
				var cl = 'log-type-title';
			} else {
				var t = 'Incident Report';
				var cl = 'ir-type-title';
			}
			h += '<div class="log-entry-wrapper"><span class="log-log-type '+cl+'">'+t+'</span><span class="log-info-sep"> | </span><span class="log-log-time">'+l.logTimestamp+'</span><br><span class="log-guard-name">'+l.guardName+'</span><span class="log-info-sep"> | </span><span class="log-category">'+l.category+'</span><span class="log-info-sep"> | </span><span class="log-log-comment">'+l.logComment+'</span></div>';
		}
		$('.log-view-wrap').html(h);
	});
}
