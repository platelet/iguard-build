//GLOBAL DECLARATION

var currentView = '';
var currentMode = '';
var currentEditId = '';
var currentViewData = [];
var logs = [];

//ON LOAD
$(function() {
	setUpTimeOuts();
	$('select').selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'}); //init select
	$('input[data-time]').datetimepicker();
	$('.side-bar-item').click(function() {
		if (!$(this).hasClass('selected')) {
			$('.side-bar-item').removeClass('selected');
			$(this).addClass('selected');
			//change tabs
			loadView($(this).attr('data-tab'), 'new', '');
		}
	}); //bind sidebar

	//bind the disable link
	$('.location-enable-link').on('click', function() {
		console.log(currentView, currentEditId);
		if (currentView != 'max-time' && currentEditId.length != 9) { return; }
		if ($(this).hasClass('enabled')) {
			updateLocationAlert(currentEditId, 0);
		} else {
			updateLocationAlert(currentEditId, 1);
		}
	});

	//bind the select all buttons
	$('.select-all').on('click', function (e) {
		var f = $(this).attr('data-for');
		var v = [];
		$('select[name="'+f+'"] option').each(function (e, i) {
			v.push($(this).val());
			$(this).attr('selected', true);
		});
		$('select[name="'+f+'"]').selectpicker('refresh').selectpicker('render');
	});

	collectLogEntries(false);
	collectClientData(false);
	collectGuardData(false);
	collectLogCategories(false);
	collectLocationData(false);
	bind(); //bind other events
	loadView('guard', 'new', ''); //load the first view in
});

function updateLocationAlert(locationId, locationAlertState) {
	currentView = '';
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/locationAlertStatus', { locationId : locationId, locationState : locationAlertState }, function(data) {
		collectMaxInts(true);
		collectLocationData(false);
	});
}


//LOAD VIEW FUNCTION AND HELPERS
function loadView(viewName, mode, id) {
	$('input').val('');
	$('input[name="filter-from"]').val(Date.now().addDays(-2).toString('yyyy/MM/dd HH:mm'));
	$('input[name="filter-to"]').val(Date.now().addDays(2).toString('yyyy/MM/dd HH:mm'));
	if (currentView != viewName) {
		$('.template').hide();
		$('.template[data-template="'+viewName+'"]').show();
		if (viewName == 'guard') {
			collectGuardData(true);
		} else if (viewName == 'client') {
			collectClientData(true);
		} else if (viewName == 'location') {
			collectLocationData(true);
		} else if (viewName == 'max-time') {
			$('#max-int-view').html('');
			$('.admin-title-sep').hide();
			$('.location-enable-link').hide().removeClass('enabled disabled').text('');
			collectMaxInts(true);
		} else if (viewName == 'logs') {
			collectLogEntries(true);
		} else if (viewName == 'setting') {
			collectSettings(true);
		} else if (viewName == 'alert') {
			collectAlerts(true);
		} else if (viewName == 'log-cat') {
			collectLogCategories(true);
		}
	}
	$('button.admin-form-action').removeClass('selected');
	if (mode == 'new') {
		$('button[btn-object="'+viewName+'"][btn-action="new"]').addClass('selected');
		$('[data-control-type="edit"]').hide();
		$('[data-control-type="new"]').show();
		$('input[name="u_pass"]').parent().show();
	} else {
		$('button[btn-object="'+viewName+'"][btn-action="edit"][btn-id="'+id+'"]').addClass('selected');
		$('[data-control-type="edit"]').show();
		$('[data-control-type="new"]').hide();
		$('input[name="u_pass"]').parent().hide();
		for (i in currentViewData) {
			var d = currentViewData[i];
			if (currentView == 'guard' && d[6] == id) {
				$('input[name="g_name"]').val(d[0]);
				$('input[name="g_num"]').val(d[1]);
				$('input[name="g_phone"]').val(d[2]);
				$('input[name="u_email"]').val(d[4]);
			} else if (currentView == 'client' && d[8] == id) {
				$('input[name="c_name"]').val(d[0]);
				$('input[name="c_n_name"]').val(d[1]);
				$('input[name="c_emails"]').val(d[2]);
				$('input[name="c_num"]').val(d[3]);
				$('input[name="c_add"]').val(d[4]);
				$('input[name="c_cit"]').val(d[5]);
				$('input[name="u_email"]').val(d[6]);
			} else if (currentView == 'location' && d[0][5] == id) {
				var l = d[0], g = d[1];
				$('input[name="l_name"]').val(l[0]);
				$('input[name="l_add"]').val(l[1]);
				$('input[name="l_cit"]').val(l[2]);
				$('[name="c"]').val(l[4]).change();
				//guards
				$('select[name="g"]').find('option').removeAttr('selected');
				for (i in g) {
					$('select[name="g"]').find('[value="'+g[i][1]+'"]').attr('selected', 'selected');
				}
				$('[name="g"]').change();
			} else if (currentView == 'max-time') {
				var lA = [];
				for (i in window.currentViewData) {
					if (currentViewData[i].logLocationId == id) {
						lA.push(currentViewData[i]);
					}
				}
				if (lA.length == 0) {
					$('#max-int-view').html('<p>there are no maximum intervals for this location');
				} else {
					var h = '';
					for (i in lA) {
						h += '<div class="m-l-wrap" data-mId="'+lA[i].maximumLogTimeId+'"><p class="conts">'+lA[i].fromDateTimeStr.replace('::', ' ') +'</p><p>until</p><p class="conts">'+lA[i].untilDateTimeStr.replace('::', ' ')+'</p><p class="int">'+lA[i].maxInterval+' '+lA[i].maxIntervalUnit+'</p><span class="fui-cross"></span></div>';
					}
					$('#max-int-view').html(h);
					bindMaxIntDelete();
				}

				for (i in window.locations) {
					if (window.locations[i][0][5] == id) {
						if (window.locations[i][0][6] == '1') {
							$('.admin-title-sep').show();
							$('.location-enable-link').show().removeClass('disabled').addClass('enabled').text('disable location alerts');
						} else {
							$('.admin-title-sep').show();
							$('.location-enable-link').show().removeClass('enabled').addClass('disabled').text('enable location alerts');
						}
					}
				}

			}
		}
	}
	bind();
	currentView = viewName
	currentMode = mode;
	currentEditId = id;
}

//LOG FILTERING
function applyAlertFilter() {
	window.filterArray = [];
	var error = false;
	$('.filter-error').hide();
	var location = getMultiSelections('filter-location');
	var from = Date.parseExact($('input[name="filter-from"]:eq(1)').val(), 'yyyy/MM/dd HH:mm');
	var to = Date.parseExact($('input[name="filter-to"]:eq(1)').val(), 'yyyy/MM/dd HH:mm');

	for (i in window.alerts) {
		var l = window.alerts[i];
		var fit = true;
		if (location.indexOf(l.locationName) == -1)
			fit = false;
		if (Date.parse(l.alertTimestamp) < from || Date.parse(l.alertTimestamp) > to)
			fit = false
		if (fit)
			window.filterArray.push(l);
	}

	if (window.filterArray.length == 0) {
		$('#alert-filtered-view').html('').append($('<p style="color: #e74c3c; font-size: 20px; text-align: center;">').text('No logs fit that filter'));
		$('#export-filter').hide();
	} else {
		var h = alertHTML(filterArray);
		$('#alert-filtered-view').html(h);
		$('#export-filter').show();
	}
}

function applyFilter() {
	var location = $('[name="filter-location"]').parent().find('.select-block').find('span.filter-option').eq(0).text().split(',').map(function (e,i) {return e.trim()});
	var category = $('[name="filter-cat"]').parent().find('.select-block').find('span.filter-option').eq(0).text().split(',').map(function (e,i) {return e.trim()});;
	var logType = getMultiSelections('filter-type').split(':');
	var guard = $('[name="filter-g"]').parent().find('.select-block').find('span.filter-option').eq(0).text().split(',').map(function (e,i) {return e.trim()});;
	var from = Date.parseExact($('input[name="filter-from"]:eq(0)').val(), 'yyyy/MM/dd HH:mm');
	var to = Date.parseExact($('input[name="filter-to"]:eq(0)').val(), 'yyyy/MM/dd HH:mm');

	var filter = {
		locations: location,
		type: logType,
		categories: category,
		guards: guard,
		from: from,
		to: to
	};
	collectLogEntries(false, filter);
}
function exportLogs() {
	window.downloader = window.open('', '_blank');
	if (window.logs.length != 0) {
		$.post('/exportLogExcel', { data : window.logs, timestamp : Date.now() }, function(data) {
			if (data) {
				console.log(data, window.location.host);
				downloader.location = '/downloadExcel/'+data;
				//a.focus();
			}
		});
	}
}
function exportAlerts() {
	if (window.filterArray.length != 0) {
		$.post('/exportAlertExcel', { data : filterArray, timestamp : Date.now() }, function(data) {
			if (data) {
				var a = window.open('http://'+window.location.host+'/downloadExcel/'+data, '_blank');
				a.focus();
			}
		});
	}
}
function alertHTML(d) {
	h = '';
	for (i in d) {
		var a = d[i];
		h += '<div class="alert-entry-wrapper"><span class="alert-location-name">'+a.locationName+'</span><span class="log-info-sep"> | </span><span class="alert-log-time">'+Date.parseExact(a.alertTimestamp, 'yyyy-MM-dd HH:mm:ss').toString('HH:mm   dd/MM/yyyy')+'</span><br><span class="alert-comment">'+a.alertComment+'</span></div>';
	}

	return h;
}
function logHTML(d) {
	var h = '';
	for (i in d) {
		var l = d[i];
		if (l.logType == '0') {
			var cl = 'log-type-title';
			var lt = 'Log Entry';
		} else {
			var cl = 'ir-type-title';
			var lt = 'Incident Report';
		}
		h += '<div class="log-entry-wrapper"><span class="log-location-name">'+l.locationName+'</span><span class="log-info-sep"> | </span><span class="log-log-type '+cl+'">'+lt+'</span><span class="log-info-sep"> | </span><span class="log-log-time">'+Date.parseExact(l.logTimestamp, 'yyyy-MM-dd HH:mm:ss').toString('HH:mm   dd/MM/yyyy')+'</span><br><span class="log-guard-name">'+l.guardName+'</span><span class="log-info-sep"> | </span><span class="log-category">'+l.category+'</span><span class="log-info-sep"> | </span><span class="log-log-comment">'+l.logComment+'</span></div>';
	}
	return h;
}
//DATA COLLECION METHODS
function collectClientData(inChargeOfLoading) {
	$.get('/admin/clients', { }, function(data) {
		var d = $.parseJSON(data);
		window.currentViewData = d;
		var h = '<small id="no-clients-text" class="no-object-alert">there are no clients</small>';
		var h2 = ''; //location page
		for (i in d) {
			h += '<button btn-object="client" btn-action="edit" btn-id="'+d[i][8]+'" class="admin-form-action btn btn-info btn-md btn-block slide-btn">'+d[i][0]+'</button>';
			h2 += '<option value="'+d[i][9]+'">'+d[i][0]+'</option>';
		}
		$('#client-button-wrap').html(h);
		$('[name="c"]').html(h2).selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'});
		if (d.length == 0) {
			$('#no-clients-text').show();
		}
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('client', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function collectLocationData(inChargeOfLoading) {
	$.get('/admin/locations', { }, function(data) {
		var d = $.parseJSON(data);
		window.currentViewData = d;
		window.locations = d;
		var h = '<small id="no-locations-text" class="no-object-alert">there are no locations</small>'; //location page
		var h2 = '<small id="no-locations-text" class="no-object-alert">there are no locations</small>'; //max time page
		var h3 = ''; //log page
		for (i in d) {
			h += '<button btn-object="location" btn-action="edit" btn-id="'+d[i][0][5]+'" class="admin-form-action btn btn-info btn-md btn-block slide-btn">'+d[i][0][0]+'</button>';
			h2 += '<button btn-object="max-time" btn-action="edit" btn-id="'+d[i][0][5]+'" class="admin-form-action btn btn-info btn-md btn-block slide-btn">'+d[i][0][0]+'</button>';
			h3 += '<option value="'+d[i][0][0]+'">'+d[i][0][0]+'</option>'
		}
		$('#location-button-wrap').html(h);
		$('#max-location-wrap').html(h2);
		$('[name="filter-location"]').html(h3).selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'});
		if (d.length == 0) {
			$('#no-locations-text').show();
		}
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('location', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function collectGuardData(inChargeOfLoading) {
	$.get('/admin/guards', { }, function(data) {
		var d = $.parseJSON(data);
		window.currentViewData = d;
		var h = '<small id="no-guards-text" class="no-object-alert">there are no guards</small>';
		var h2 = ''; //log page
		var h3 = ''; //location
		for (i in d) {
			h += '<button btn-object="guard" btn-action="edit" btn-id="'+d[i][6]+'" class="admin-form-action btn btn-info btn-md btn-block slide-btn">'+d[i][0]+'</button>';
			h2 += '<option value="'+d[i][0]+'">'+d[i][0]+'</option>';
			h3 += '<option value="'+d[i][7]+'">'+d[i][0]+'</option>';
		}
		$('#guard-button-wrap').html(h);
		$('[name="filter-g"]').html(h2).selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'});
		$('[name="g"]').html(h3).selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'});
		if (d.length == 0) {
			$('#no-guards-text').show();
		}
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('guard', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function collectLogCategories(inChargeOfLoading) {
	$.get('/admin/categories', { }, function(data) {
		var d = $.parseJSON(data);
		window.currentViewData = d;
		var h = '';
		var h2 = ''; //log page
		for (i in d) {
			h += '<li class="log-cat-h" data-id="'+d[i][0]+'" data-index="'+d[i][2]+'"><span class="fui-list"></span>'+d[i][1]+'</li>';
			h2 += '<option value="'+d[i][1]+'">'+d[i][1]+'</option>';
		}
		$('#log-cat-list').html(h).sortable().disableSelection();
		$('[name="filter-cat"]').html(h2).selectpicker({style: 'btn-lg btn-primary btn-block', menuStyle: 'dropdown-inverse'});
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('log-cat', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function collectLogEntries(inChargeOfLoading, filter) {
	if (filter) {
		$.get('/admin/logs', {
			filter: true,
			locations: filter.locations,
			type: filter.type,
			categories: filter.categories,
			guards: filter.guards,
			timestampFrom: moment(filter.from).format('YYYY-MM-DD H:m:ss'),
			timestampTo: moment(filter.to).format('YYYY-MM-DD H:m:ss')
		}, function(data) {
			var d = $.parseJSON(data);
			window.logs = d;
			var h = logHTML(window.logs);
			$('#log-filtered-view').html(h);
		});
	} else {
		$.get('/admin/logs', {filter: false}, function(data) {
			var d = $.parseJSON(data);
			window.logs = d;
			var h = logHTML(window.logs);
			$('#log-filtered-view').html(h);
		});
	}
}
function collectAlerts(inChargeOfLoading) {
	$.get('/admin/alerts', { }, function(data) {
		var d = $.parseJSON(data);
		window.alerts = d;
		var h = alertHTML(window.alerts);
		$('#alert-filtered-view').html(h);
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('alert', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function collectMaxInts(inChargeOfLoading) {
	$.get('/admin/maxInts', {}, function(data) {
		var d = $.parseJSON(data);
		window.currentViewData = d;
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('max-time', 'new', '');
				$('.loading-screen').fadeOut(300);
			}, 300);
		}
	});
}
function bindMaxIntDelete() {
	$('.m-l-wrap span').click(function() {
		var id = $(this).parent().attr('data-mId');
		currentView = '';
		$('.loading-screen').fadeIn(300);
		$('.template').hide();
		$.post('/admin/deleteMaxInt', { objectId : id }, function(data) {
			collectMaxInts(true);
		});
	});
}
function bindAdminIrDelete() {
	$('.adm-wrap span').click(function() {
		var id = $(this).parent().attr('data-id');
		currentView = '';
		$('.loading-screen').fadeIn(300);
		$('.template').hide();
		$.post('/admin/deleteAdmin', { objectId : id }, function(data) {
			collectSettings(true);
		});
	});
	$('.ir-email-wrap span').click(function() {
		var id = $(this).parent().attr('data-id');
		currentView = '';
		$('.loading-screen').fadeIn(300);
		$('.template').hide();
		$.post('/admin/deleteIrEmail', { objectId : id }, function(data) {
			collectSettings(true);
		});
	});
	$('.alert-email-wrap span').click(function() {
		var id = $(this).parent().attr('data-id');
		currentView = '';
		$('.loading-screen').fadeIn(300);
		$('.template').hide();
		$.post('/admin/deleteAlertEmail', { objectId : id }, function(data) {
			collectSettings(true);
		});
	});
}
function collectSettings(inChargeOfLoading) {
	//fetch emails
	$.get('/admin/irEmails', {}, function(data) {
		window.irEmails = $.parseJSON(data);
		var h = '';
		for (i in window.irEmails) {
			var j = window.irEmails[i];
			h += '<div class="ir-email-wrap" data-id="'+j.incidentReportEmailId+'"><p>'+j.IREmail+'</p><span class="fui-cross"></span></div>';
		}

		$('#ir-email-view').html(h);
	});
	$.get('/admin/alertEmails', {}, function(data) {
		window.alertEmails = $.parseJSON(data);
		var h = '';
		for (i in window.alertEmails) {
			var j = window.alertEmails[i];
			h += '<div class="alert-email-wrap" data-id="'+j.alertEmailId+'"><p>'+j.alertEmail+'</p><span class="fui-cross"></span></div>';
		}

		$('#alert-email-view').html(h);
	});
	//fetch admins
	$.get('/admin/admins', {}, function(data) {
		window.admins = $.parseJSON(data);
		var h = '';
		for (i in window.admins) {
			var j = window.admins[i];
			h += '<div class="adm-wrap" data-id="'+j.userId+'"><p>'+j.username+'</p><p>'+j.email+'</p><span class="fui-cross"></span></div>';
		}

		$('#adm-view').html(h);
	});
	//fetch low admins
	$.get('/admin/Ladmins', {}, function(data) {
		window.Ladmins = $.parseJSON(data);
		var h = '';
		for (i in window.Ladmins) {
			var j = window.Ladmins[i];
			h += '<div class="adm-wrap" data-id="'+j.userId+'"><p>'+j.username+'</p><p>'+j.email+'</p><span class="fui-cross"></span></div>';
		}

		$('#l-adm-view').html(h);
	});

	setTimeout(function() {
		if (inChargeOfLoading) {
			setTimeout(function() {
				loadView('setting', 'new', '');
				$('.loading-screen').fadeOut(300);
				bindAdminIrDelete();
			}, 300);
		}
	}, 800);
}
//DATA CREATE METHODS
function createGuard() {
	var data = checkForm('guard-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createGuard', { 'formData' : data }, function(data) {
		console.log(data);
		collectGuardData(true);
	});
}
function createClient() {
	var data = checkForm('client-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createClient', { 'formData' : data }, function(data) {
		collectClientData(true);
	});
}
function createLocation() {
	var data = checkForm('location-form');
	data[data.length-1] = { name : 'guards', 'value' : getMultiSelections('g') };
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createLocation', { 'formData' : data }, function(data) {
		collectLocationData(true);
	});
}
function createMaxTime() {
	var l = currentEditId;
	//form data
	var day_from = getMultiSelections('w-day-from');
	var day_to = getMultiSelections('w-day-to');
	var time_from = getMultiSelections('time-from');
	var time_to = getMultiSelections('time-to');
	//time values
  var time_unit = getMultiSelections('time-unit'), time_value = (time_unit === 'hours') ? getMultiSelections('time-val')*60 : getMultiSelections('time-val');

	var data = {
		location : l,
		start : day_from + '::' + time_from,
		finish : day_to + '::' + time_to,
		tu : time_unit,
		tv : time_value
	};

  var day_values = Array(Date.getDayNumberFromName(day_from), Date.getDayNumberFromName(day_to));

  var day_dif = Math.abs(day_values[0] - day_values[1]);

  time_values = Array(time_from.split(':'), time_to.split(':'));
  time_values = Array(Array(parseInt(time_values[0][0]), parseInt(time_values[0][1])), Array(parseInt(time_values[1][0]), parseInt(time_values[1][1])));

  var time_dif = ((time_values[1][0] - time_values[0][0]) * 60) +
      (time_values[1][1] - time_values[0][1]);

  var error = false;

  if (day_dif === 0) {
    if (time_dif < time_value) {
      alert("Time interval to large for time range");
    }
  } else if (day_dif === 1) {
    var time_dif = (((24 - time_values[0][0]) + time_values[1][0]) * 60) -
        time_values[0][1] + time_values[1][1];
    if (time_dif < time_value) {
      alert("Time interval to large for time range");
    }
  }

  currentView = '';
  if (error) {return;}
  $('.loading-screen').fadeIn(300);
  $('.template').hide();
  $.post('/admin/createMaxInt', { formData : data }, function(data) {
    collectMaxInts(true);
  });
}
function createLogCat() {
	if ($('[name="cat-name"]').val().length > 0) {
		$('[name="cat-name"]').parent().removeClass('has-error');
		$('.loading-screen').fadeIn(300);
		$('.template').hide();
		currentView = '';
		$.post('/admin/createLogCat', { 'catName' : $('[name="cat-name"]').val() }, function(data) {
			collectLogCategories(true);
		});
	} else {
		$('[name="cat-name"]').parent().addClass('has-error');
	}
}
function createAdmin() {
	var data = checkForm('admin-account-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createAdmin', { 'formData' : data }, function(data) {
		collectSettings(true);
	});
}
function createLAdmin() {
	var data = checkForm('low-admin-account-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createLAdmin', { 'formData' : data }, function(data) {
		collectSettings(true);
	});
}
function createEmail() {
	var data = checkForm('ir-email-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createIrEmail', { 'formData' : data }, function(data) {
		collectSettings(true);
	});
}
function createAlertEmail() {
	var data = checkForm('alert-email-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/createAlertEmail', { 'formData' : data }, function(data) {
		collectSettings(true);
	});
}

//DATA UPDATE METHODS
function saveClient() {
	$('[name="u_pass"]').val('false');
	var data = checkForm('client-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/saveClient', { 'formData' : data, 'clientId' : currentEditId }, function(data) {
		collectClientData(false);
		loadView('client', 'edit', currentEditId);
	});
}
function saveGuard() {
	$('[name="u_pass"]').val('false');
	var data = checkForm('guard-form');
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/saveGuard', { 'formData' : data, 'guardId' : currentEditId }, function(data) {
		collectGuardData(false);
		loadView('guard', 'edit', currentEditId);
	});
}
function saveLocation() {
	var data = checkForm('location-form');
	data[data.length-1] = { name : 'guards', 'value' : getMultiSelections('g') };
	currentView = '';
	if (!data) {return;}
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/saveLocation', { 'formData' : data, 'locationId' : currentEditId }, function(data) {
		collectLocationData(true);
		loadView('location', 'edit', currentEditId);
	});
}
function saveCatOrder() {
	var categories = [];
	$('.log-cat-h').each(function() {
		categories.push($(this).attr('data-id'));
	});
	currentView = '';
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
	$.post('/admin/saveCategoryOrder', { 'formData' : categories }, function(data) {
		loadView('log-cat', 'new', '');
	});
}
//DELETE METHODS
function deleteClient() {
	if (currentEditId == '') {return;}
	setLoading();
	$.post('/admin/deleteClient', { objectId : currentEditId }, function(data) {
		collectClientData(true);
	});
}
function deleteGuard() {
	if (currentEditId == '') {return;}
	setLoading();
	$.post('/admin/deleteGuard', { objectId : currentEditId }, function(data) {
		collectGuardData(true);
	});
}
function deleteLocation() {
	if (currentEditId == '') {return;}
	setLoading();
	$.post('/admin/deleteLocation', { objectId : currentEditId }, function(data) {
		collectLocationData(true);
	});
}
//HELPER METHODS
function setLoading() {
	currentView = '';
	$('.loading-screen').fadeIn(300);
	$('.template').hide();
}
function getMultiSelections(n) {
	var d = '';
	$('[name="'+n+'"]').parent().find('.select-block').find('ul').find('.selected').each(function() {
		var a = $('[name="'+n+'"]').find(':contains('+$(this).children().eq(0).text()+')').val();
		d += a + ':';
	});
	return d.substr(0, d.length-1);
}
function bind() {
	$('button.admin-form-action').click(function() {
		if (!$(this).hasClass('selected')) {
			$('button.admin-form-action').removeClass('selected');
			$(this).addClass('selected');
			loadView($(this).attr('btn-object'), $(this).attr('btn-action'), $(this).attr('btn-id'));
		}
	});
}


function checkForm(formName) {
	var fE = false;
	$('#'+formName).find('input').parent().removeClass('has-error'); //reset form errors
	$('#'+formName).find('input').each(function() {
		var e = false;
		var v = $(this).val();
		var t = $(this).attr('data-type');
		if (t == 'var' && v.length <= 0) {
			e = true;
		} else if (t == 'phone' && parseInt(v) == NaN) {
			e = true;
		} else if (t == 'email' && validateEmail(v) == false) {
			e = true;
		} else if (t == 'number' && parseInt(v) == NaN) {
			e = true;
		} else if (t == 'email-sep') {
			var j = $(this).val().split('; ');
			for (i in j) {
				if (!validateEmail(j[i])) {
					e = true;
				}
			}
		}

		if (e) {$(this).parent().addClass('has-error'); fE = true;}
	});

	return (fE) ? false : $('#'+formName).serializeArray();
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function setUpTimeOuts() {
	//simple time units
	var h = '';
	for (var i=1;i<91;i++) {
		if (i == 30) {
			h += '<option value="'+i+'" selected="">'+i+'</option>'; //select 30 by default
		} else {
			h += '<option value="'+i+'">'+i+'</option>';
		}
	}
	$('[name="time-val"]').html(h);

	//between times
	var h = '';
	for (var z=0;z < 24;z++) {
		for (var m=0;m<2;m++) {
			var t = hours(z.toString()) + ':' + minutes(m);
			h += '<option value="'+t+'">'+t+'</options>';
		}
	}
	$('[name="time-to"]').html(h);
	$('[name="time-from"]').html(h);
}
function hours(h) {
	if (h.length == 1) {
		return '0' + h.toString();
	} else {
		return h;
	}
}
function minutes(m) {
	if (m%2 == 0) {
		return '00';
	} else {
		return '30';
	}
}
