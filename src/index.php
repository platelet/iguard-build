<?php

	require('base.php');
	require('scripts/function_helper.php');
	require('scripts/excel_exporter.php');
	require('scripts/renderFunctions.php');
	require 'Mail/PHPMailerAutoload.php';
	require('scripts/mail_helper.php');
	require('flight/Flight.php');
	require('scripts/Mobile_Detect.php');

	$GLOBALS['user_log_in_status'] = isUserLoggedIn();

	//set timezone
	date_default_timezone_set('pacific/auckland');

	function displayLogin($username, $alert, $alertStrong, $alertType, $alertMessage, $r) {
		renderPage('main_login', array('username' => $username, 'alert' => $alert, 'alertStrong' => $alertStrong, 'alertType' => $alertType, 'alertMessage' => $alertMessage), 'pageContent');
		renderLayout('iGuard | login', '', false, false, $r);
	}
	function renderLoginWithError($eId) {
		if ($eId == 0) {
			//not logged in
			displayLogin('', true, 'Error!', 'danger', 'you are not logged in, please log in', routeExtensionFromURL(Flight::request()->url));
		} else if ($eId == -1) {
			//session timeout
			displayLogin('', true, 'Error!', 'danger', 'your session timed-out, please log in', routeExtensionFromURL(Flight::request()->url));
		} else if ($eId == -2) {
			//normal
			displayLogin('', false, '', '', '', '');
		} else if ($eId == 1) {
			//password changed
			displayLogin('', true, 'Success!', 'success', 'Your password was changed', '../../');
		} else if ($eId == 6) {
			//non admin
			displayLogin('', true, 'Error!', 'danger', 'You are not authorised to view that', '');
		}
	}
	function routeExtensionFromURL($url) {
		$directories = substr_count($url, '/');
		$r = '';
		for ($i = 0;$i < $directories;$i++) {
			$r .= '../';
		}
		return $r;
	}

	/* home page */
	Flight::route('GET /', function() {
		//check if logged in
		if ($GLOBALS['user_log_in_status'] == 1) {
			if ($_SESSION['accountLevel'] >= 2) {
				//admin user
				Flight::redirect('/admin');
			} else {
				//display locations
				Flight::redirect('/locations');
			}
		} else {
			//reroute to login page
			renderLoginWithError(-2);
		}
	});

	Flight::route('GET /logout', function() {
		//logout user
		logUserOut();
		//redirect to home
		Flight::redirect('/');
	});

	Flight::route('POST /login', function() {
		//check email and password were given
		$loginData = Flight::request()->data;
		//check if details are correct
		$logIn = checkUserDetails($loginData->username, $loginData->password);
		if ($logIn == true && $logIn['accountType'] < 2) {
			logInUser($logIn['userId'], $logIn['accountId'], $logIn['username'], $logIn['email'], $logIn['accountType']);
			if (Flight::request()->url != '/login') {
				Flight::redirect(Flight::request()->url); //goto timeout page
			} else {
				Flight::redirect('/locations'); //default login page
			}
		} else if ($logIn == true && $logIn['accountType'] >= 2) {
			logInUser($logIn['userId'], null, $logIn['username'], $logIn['email'], $logIn['accountType']);
			Flight::redirect('/admin');
		} else {
			displayLogin($loginData->username, true, 'Error!', 'danger', 'login details incorrect, try again', '');
		}
	});

	Flight::route('GET /account', function() {
		//check if logged in
		if ($GLOBALS['user_log_in_status'] == 1) {
			$data = getUserDetails($_SESSION['id']);
			renderPage('main_account', array('username'=>$_SESSION['username'],'accountType'=>$_SESSION['accountLevel'],'data'=>$data), 'pageContent');
			renderLayout('iGuard | account', 'flaticon-user-shadow', true, true, '');
		} else {
			//reroute to login page
			renderLoginWithError($GLOBALS['user_log_in_status']);
		}
	});
	Flight::route('GET /forgot', function() {
		logUserOut(); //stop any unwanted errors
		//display forgot page
		renderPage('main_forgot', array('alert' => false, 'alertStrong' => '', 'alertType' => '', 'alertMessage' => ''), 'pageContent');
		renderLayout('iGuard | forgot', '', false, false, '');
	});
	Flight::route('POST /forgot', function() {
		//make sure a email was presented
		$username = Flight::request()->data->username;
		if ($username != '') {
			$user = doesUsernameExist($username);
			if ($user != false) {
				//send reset email
				if (sendResetPasswordFor($user)) {
					renderPage('main_forgot', array('alert' => true, 'alertStrong' => 'Success!', 'alertType' => 'success', 'alertMessage' => "A reset email has been sent"), 'pageContent');
				} else {
					renderPage('main_forgot', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => "An error occurred, please try again"), 'pageContent');
				}
				renderLayout('iGuard | forgot', '', false, false, '');
			} else {
				//username does not exist
				renderPage('main_forgot', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => "That username doesn't exist"), 'pageContent');
				renderLayout('iGuard | forgot', '', false, false, '');
			}
		} else {
			//no username was supplies
			renderPage('main_forgot', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => 'Please provide a valid username'), 'pageContent');
			renderLayout('iGuard | password reset', '', false, false, '');
		}
	});
	Flight::route('GET /reset/@resetId', function($resetId) {
		logUserOut(); //stop any unwanted errors
		print_r($resetId);
		if (doesResetCodeExist($resetId)) {
			renderPage('main_reset', array('alert' => false, 'alertStrong' => '', 'alertType' => '', 'alertMessage' => ''), 'pageContent');
			renderLayout('iGuard | password reset', '', false, false, '../');
		} else {
			renderPage('main_reset', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => 'That code is invalid'), 'pageContent');
			renderLayout('iGuard | password reset', '', false, false, '../');
		}
	});
	Flight::route('POST /reset/@resetId', function($resetId) {
		if (doesResetCodeExist($resetId)) {
			//check if passwords match
			$data = Flight::request()->data;
			if (isset($data->p_a) && isset($data->p_b) && $data->p_a == $data->p_b) {
				//reset the password
				resetPassword($resetId, $data->p_a);
				//renderPage('main_reset', array('alert' => true, 'alertStrong' => 'Success!', 'alertType' => 'success', 'alertMessage' => 'Your password was changed'), 'pageContent');
				//renderLayout('iGuard | password reset', '', false, false, '../');
				renderLoginWithError(1);
			} else {
				renderPage('main_reset', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => 'Passwords must match'), 'pageContent');
				renderLayout('iGuard | password reset', '', false, false, '../');
			}
		} else {
			renderPage('main_reset', array('alert' => true, 'alertStrong' => 'Error!', 'alertType' => 'danger', 'alertMessage' => 'That code is invalid'), 'pageContent');
			renderLayout('iGuard | password reset', '', false, false, '../');
		}
	});

	Flight::route('GET /logs', function() {
		if ($_SESSION['accountLevel'] == 0) {
			Flight::redirect('/locations');
		}
		if ($GLOBALS['user_log_in_status'] == 1) {
			$locations = getLocationsForUser();
			$categories = getLogCategories();
			$guards = getSimpleGuards();

			renderPage('main_alerts', array( 'lg' => getAllLogsForUser(), 'locations' => $locations, 'categories' => $categories, 'guards' => $guards, 'accLvl' => $_SESSION['accountLevel']), 'pageContent');
			renderLayout('iGuard | logs', 'flaticon-calendar-alert', true, true, '');
		} else {
			//redirect to login page
			renderLoginWithError($GLOBALS['user_log_in_status']);
		}
	});

	Flight::route('POST /exportLogExcel', function() {
		//get json data
		$data = Flight::request()->data;
		//print result to POST call
		print_r(generateLogExcel($data->data, $data->timestamp));
	});
	Flight::route('POST /exportAlertExcel', function() {
		//get json data
		$data = Flight::request()->data;
		//print result to POST call
		print_r(generateAlertExcel($data->data, $data->timestamp));
	});
	Flight::route('GET /downloadExcel/@csvId', function($csvId) {
		//download the file with forced headers
		downloadCSVFile($csvId);
		die();
	});

	/* location routes */
	Flight::route('GET /locations', function() {
		if ($GLOBALS['user_log_in_status'] == 1) {
			//display the locations page
			$data = getLocationsForUser();
			renderPage('main_selection', array('locations' => $data), 'pageContent');
			renderLayout('iGuard | locations', 'flaticon-map-location', true, true, '');
		} else {
			//reroute to login page
			renderLoginWithError($GLOBALS['user_log_in_status']);
		}
	});

	Flight::route('GET /locations/@locationId', function($locationId) {
		$detect = new Mobile_Detect;
		if ($GLOBALS['user_log_in_status'] == 1) {
			if (canUserViewLocation($locationId)) {
				//if user is a guard (save timestamp + guardId)
				if ($_SESSION['accountLevel'] == 1) {
				    logGuardLocationAccess($_SESSION['aId'], $locationId);
				}

				//display the location page
				$data = getLocationData($locationId); //location/user data
				$mes = getMessagesForLocation($locationId);
				$cat = getLogCategories(); //log categories
				$log = getLogsForLocation($locationId); //log entries
				$int = getMaxIntervalsForLocation($locationId); //max intervals
				renderPage('main_location', array('l' => $data[0], 'u' => $data[1], 'c' => $cat, 'mes' => $mes, 's' => $_SESSION, 'lg' => $log, 'mi' => $int, 'mo' => $detect->isMobile()), 'pageContent');
				renderLayout('iGuard | '.$data[0]['locationName'], 'flaticon-map-location', true, true, '../');
			} else {
				echo 'You cannot view that location due to account restrictions';
			}
		} else {
			//reroute to login page
			renderLoginWithError($GLOBALS['user_log_in_status']);
		}
	});

	/* MESSAGES */
	Flight::route('POST /newMessage', function() {
		if ($GLOBALS['user_log_in_status'] == 1) {
			$data = Flight::request()->data;
			print_r(createMessage($data));
		} else {
			return false;
		}
	});
	Flight::route('POST /getMessages', function() {
		if ($GLOBALS['user_log_in_status'] == 1) {
			$data = Flight::request()->data;
			print_r(getJSONMessagesForLocation($data['location']));
		} else {
			return false;
		}
	});

	/* LOGS */
	Flight::route('POST /createLog', function() {
		if ($GLOBALS['user_log_in_status'] == 1) {
			$data = postArrayToAssocArray(Flight::request()->data->formData);
			$timestamp = Flight::request()->data->timestamp;
			$type = Flight::request()->data->type;
			$location = Flight::request()->data->location;
			$a = array($data, $timestamp, $type, $location);
			print_r(createLogEntry($a));
		} else {
			return false;
		}
	});
	Flight::route('POST /getLogs', function() {
		if ($GLOBALS['user_log_in_status'] == 1) {
			$lId = Flight::request()->data->location;
			print_r(json_encode(getLogsForLocation($lId)));
		} else {
			return false;
		}
	});

	/* ADMIN */
	//base route
	Flight::route('GET /admin', function() {
		//make sure user is logged in and is an admin
		if ($GLOBALS['user_log_in_status'] == 1) {
			if ($_SESSION['accountLevel'] >= 2) {
				Flight::render('main_admin', array('al' => $_SESSION['accountLevel'], 'clients' => getSimpleClients(), 'guards' => getSimpleGuards(), 'categories' => getLogCategories()));
			} else {
				Flight::redirect('/locations');
			}
		} else {
			renderLoginWithError($GLOBALS['user_log_in_status']);
		}
	});
	//data routes
	Flight::route('GET /admin/guards', function() {
		print_r(getAllGuards());
	});
	Flight::route('GET /admin/clients', function() {
		print_r(getAllClients());
	});
	Flight::route('GET /admin/locations', function() {
		print_r(getAllLocations());
	});
	Flight::route('GET /admin/categories', function() {
		print_r(json_encode(getLogCategories()));
	});
	Flight::route('GET /admin/logs', function() {
		$filter = Flight::request()->query['filter'];
		if ($filter === 'true') {
			$locations = Flight::request()->query['locations'];
			$type = Flight::request()->query['type'];
			$categories = Flight::request()->query['categories'];
			$guards = Flight::request()->query['guards'];
			$from = Flight::request()->query['timestampFrom'];
			$to = Flight::request()->query['timestampTo'];
			$filtered = getFilteredLogs($locations, $type, $categories, $guards, $from, $to);
			print_r($filtered);
		} else {
			print_r(getAllLogs());
		}
	});
	Flight::route('GET /admin/alerts', function() {
		print_r(getAllAlerts());
	});
	Flight::route('GET /admin/maxInts', function() {
		print_r(getAllMaxIntervals());
	});
	Flight::route('GET /admin/admins', function() {
		print_r(getAllAdmins());
	});
	Flight::route('GET /admin/Ladmins', function() {
		print_r(getAllLAdmins());
	});
	Flight::route('GET /admin/irEmails', function() {
		print_r(getAllIrEmails());
	});
	Flight::route('GET /admin/alertEmails', function() {
		print_r(getAllAlertEmails());
	});
	//creation routes
	Flight::route('POST /admin/createClient', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r(createClient($data));
	});
	Flight::route('POST /admin/createGuard', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r($data);
		print_r(createGuard($data));
	});
	Flight::route('POST /admin/createLocation', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		$lId = createLocation($data);
		print_r(createLocationGuards($lId, $data['guards']));
	});
	Flight::route('POST /admin/createLogCat', function() {
		$data = Flight::request()->data->catName;
		print_r(createLogCategory($data));
	});
	Flight::route('POST /admin/createMaxInt', function() {
		$data = Flight::request()->data->formData;
		print_r(createMaxInterval($data));
	});
	Flight::route('POST /admin/createAdmin', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r(createAdmin($data));
	});
	Flight::route('POST /admin/createLAdmin', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r(createLAdmin($data));
	});
	Flight::route('POST /admin/createIrEmail', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r(createIrEmail($data));
	});
	Flight::route('POST /admin/createAlertEmail', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		print_r(createAlertEmail($data));
	});
	//save routes
	Flight::route('POST /admin/saveClient', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		$id = Flight::request()->data->clientId;
		print_r(saveClient($data, $id));
	});
	Flight::route('POST /admin/saveGuard', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		$id = Flight::request()->data->guardId;
		print_r(saveGuard($data, $id));
	});
	Flight::route('POST /admin/saveLocation', function() {
		$data = postArrayToAssocArray(Flight::request()->data->formData);
		$id = Flight::request()->data->locationId;
		$l = saveLocation($data, $id); //save location
		print_r(createLocationGuards($id, $data['guards'])); //re add the guards
	});
	Flight::route('POST /admin/saveCategoryOrder', function() {
		$data = Flight::request()->data->formData;
		print_r(saveLogCategoryOrder($data));
	});
	Flight::route('POST /admin/locationAlertStatus', function() {
		$data = Flight::request()->data;
		print_r(updateLocationStatus($data->locationId, $data->locationState));
	});
	//deletion methods
	Flight::route('POST /admin/deleteClient', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteClient($id));
	});
	Flight::route('POST /admin/deleteGuard', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteGuard($id));
	});
	Flight::route('POST /admin/deleteLocation', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteLocation($id));
	});
	Flight::route('POST /admin/deleteMaxInt', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteMaximumLogInt($id));
	});
	Flight::route('POST /admin/deleteAdmin', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteAdmin($id));
	});
	Flight::route('POST /admin/deleteIrEmail', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteIrEmail($id));
	});
	Flight::route('POST /admin/deleteAlertEmail', function() {
		$id = Flight::request()->data->objectId;
		print_r(deleteAlertEmail($id));
	});

	Flight::start();

?>
