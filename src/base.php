<?php

	//restart any current browser sessions
	session_start();

	/* MYSQL DATABASE CONNECTION */
	//daabase creditails
	define('DB_USER',getenv('DATABASE_USER'));
	define('DB_PASS',getenv('DATABASE_PASSWORD'));
	define('DB_NAME',getenv('DATABASE_NAME'));
	define('DB_HOST', 'mysql');

	// define('DB_USER', 'root');
	// define('DB_PASS', 'root');
	// define('DB_NAME', 'iGuard');
	// define('DB_HOST', 'localhost');

	//connect to MySQL
	$r = mysql_connect(DB_HOST, DB_USER, DB_PASS);
	$r2 = mysql_select_db(DB_NAME);

	function logGuardLocationAccess($guardId, $locationId) {
		//remove all current location entries
		$query = "DELETE FROM LastLocationAccess WHERE locationId = '".$locationId."'";
		mysql_query($query);
		//add new guard access entry
		date_default_timezone_set('pacific/auckland');
		$timestamp = date('Y-m-d H:i:s');
		$query = "INSERT INTO LastLocationAccess(lastLocationAccessId, locationId, guardId, lastAccessTimestamp) VALUES(ObjectId(), '".$locationId."', '".$guardId."', '".$timestamp."')";
		mysql_query($query);
	}


	/* USER METHODS */
		/* LOGIN */
		function checkUserDetails($un, $pw) {
			$query = "SELECT * FROM User WHERE username = '" .$un. "' AND password = '" .hash('sha256', $pw). "'";
			$res = mysql_query($query);
			if (mysql_num_rows($res) >= 1) {
				return mysql_fetch_assoc($res);
			} else {
				return false;
			}
		}
		function logInUser($id, $aId, $un, $em, $lvl) {
			//assign session parameters
			$_SESSION['id'] = $id; //userId
			$_SESSION['aId'] = $aId; //client/guard Id
			$_SESSION['username'] = $un; //username
			$_SESSION['email'] = $em; //user email
			$_SESSION['accountLevel'] = $lvl; //user account level (0,2,-1)
			$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
		}
		function getUserDetails($id) {
			if ($_SESSION['accountLevel'] == 1) {
				$query = "SELECT * FROM User, Guard WHERE Guard.guardId = User.accountId AND User.userId = '".$id."' ORDER BY User.username ASC";
			} else if ($_SESSION['accountLevel'] == 0) {
				$query = "SELECT * FROM User, Client WHERE Client.clientId = User.accountId AND User.userId = '".$id."' ORDER BY User.username ASC";
			} else {
				return false;
			}
			$res = mysql_query($query);
			return mysql_fetch_assoc($res);
		}
		function logUserOut() {
			//make sure the session is in use
			if (isset($_SESSION)) {
				//destroy current session
				session_unset();     // unset $_SESSION variable for the run-time
				session_destroy();   // destroy session data in storage
			}

		}
		function isUserLoggedIn() {
			if (isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['email']) && isset($_SESSION['accountLevel'])) {
				//check the last actitivty time
				if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3900)) {
				    // last request was more than 30 minutes ago
				    logUserOut();
				    $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
				    return -1; //session has timed out
				} else {
					return 1; //logged in
				}
			}
			$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
			return 0; //not logged in at all
		}

		/* FORGOT */
		function doesUsernameExist($un) {
			$query = "SELECT userId, email, username FROM User WHERE username = '".$un."'";
			$res = mysql_query($query);
			if (mysql_num_rows($res) >= 1) {
				return mysql_fetch_assoc($res);
			} else {
				return false;
			}
		}
		function doesResetCodeExist($c) {
			$query = "SELECT * FROM User__ResetKey WHERE resetToken = '".$c."'";
			$res = mysql_query($query);
			if (mysql_num_rows($res) == 1) {
				return true;
			} else {
				return false;
			}
		}
		function resetPassword($c, $p) {
			$query = "SELECT User.userId FROM User, User__ResetKey WHERE User.userId = User__ResetKey.userId AND User__ResetKey.resetToken = '".$c."'";
			$res = mysql_query($query);
			$id = mysql_fetch_assoc($res)['userId'];
			$query = "UPDATE User SET password = '".hash('sha256', $p)."' WHERE userId = '".$id."'";
			mysql_query($query);
			$query = "DELETE FROM User__ResetKey WHERE resetToken = '".$c."'";
			mysql_query($query);
			return true;
		}

	/* USER CREATION METHODS */
	function createClient($clientData) {
		//get client id
		$res = mysql_query('SELECT ObjectId() AS Id');
		$id = mysql_fetch_object($res)->Id;
		//create the client object first
		$query = "INSERT INTO Client(clientId, clientName, contactName, contactEmails, clientPhone, clientAddress, clientCity) VALUES('".$id."','".$clientData['c_name']."','".$clientData['c_n_name']."','".$clientData['c_emails']."', '".$clientData['c_num']."', '".$clientData['c_add']."','".$clientData['c_cit']."')";
		$res = mysql_query($query);
		//create user and associate account with user
		$queryd = "INSERT INTO User(userId, username, password, email, accountType, accountId) VALUES(ObjectId(), '" . $clientData['c_name'] . "', '" . hash('sha256', $clientData['u_pass']) . "' ,'" . $clientData['u_email'] . "', 0, '" . $id . "')";
		$res = mysql_query($queryd);
		return true;
	}
	function createGuard($guardData) {
		//get guard id
		$res = mysql_query('SELECT ObjectId() AS Id');
		$id = mysql_fetch_object($res)->Id;
		//create the guard object first
		$query = "INSERT INTO Guard(guardId, guardName, guardNumber, guardPhone) VALUES('".$id."','".$guardData['g_name']."','".$guardData['g_num']."','".$guardData['g_phone']."')";
		$res = mysql_query($query);
		//create user and associate account with user
		$queryd = "INSERT INTO User(userId, username, password, email, accountType, accountId) VALUES(ObjectId(), '" . $guardData['g_name'] . "', '" . hash('sha256', $guardData['u_pass']) . "' ,'" . $guardData['u_email'] . "', 1, '" . $id . "')";
		$res = mysql_query($queryd);
		return true;
	}
	function createLocation($locationData) {
		//get location id
		$res = mysql_query('SELECT ObjectId() AS Id');
		$id = mysql_fetch_object($res)->Id;
		//create the location object first
		$query = "INSERT INTO Location(locationId, locationName, locationAddress, locationCity, locationClientId) VALUES('".$id."','".$locationData['l_name']."','".$locationData['l_add']."','".$locationData['l_cit']."','".$locationData['c']."')";
		$res2 = mysql_query($query);
		return $id;
	}
	function createLocationGuards($lId, $guards) {
		//delete all guards currently on location
		$query = "DELETE FROM Location__Guard WHERE locationId = '".$lId."'";
		mysql_query($query);
		$guards = explode(':', $guards);
		foreach ($guards as $g) {
			//get location__guard id
			$res = mysql_query('SELECT ObjectId() AS Id');
			$id = mysql_fetch_object($res)->Id;
			//create the location object first
			$query = "INSERT INTO Location__Guard(location__guardId, locationId, guardId) VALUES('".$id."','".$lId."','".$g."')";
			$res2 = mysql_query($query);
		}
		return true;
	}

	/* GUARDS */
	function getAllGuards() {
		$query = 'SELECT Guard.guardName, Guard.guardNumber, Guard.guardPhone, User.username, User.email, User.password, User.userId, Guard.guardId FROM User, Guard WHERE Guard.guardId = User.accountId ORDER BY User.username ASC';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, $row);
		}
		$assoc_array = json_encode($assoc_array);
		return $assoc_array;
	}
	function getSimpleGuards() {
		$query = 'SELECT Guard.guardName, Guard.guardId FROM Guard ORDER BY Guard.guardName ASC';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}

	/* CLIENTS */
	function getAllClients() {
		$query = 'SELECT Client.clientName, Client.contactName, Client.contactEmails, Client.clientPhone, Client.clientAddress, Client.clientCity, User.email, User.password, User.userId, Client.clientId FROM User, Client WHERE Client.clientId = User.accountId ORDER BY User.username ASC';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, $row);
		}
		$assoc_array = json_encode($assoc_array);
		return $assoc_array;
	}
	function getSimpleClients() {
		$query = 'SELECT Client.clientName, Client.clientId FROM Client ORDER BY Client.clientName ASC';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}

	/* LOCATION */
	function getAllLocations() {
		$query = 'SELECT Location.locationName, Location.locationAddress, Location.locationCity, Client.clientName, Client.clientId, Location.locationId, Location.locationAlertsEnabled FROM Location, Client WHERE Client.clientId = Location.locationClientId ORDER BY Location.locationName ASC';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			$g = guardsForLocation($row[5]);
			array_push($assoc_array, array($row, $g));
		}
		$assoc_array = json_encode($assoc_array);
		return $assoc_array;
	}
	function guardsForLocation($locationId) {
		$query = 'SELECT Guard.guardName, Guard.guardId FROM Guard, Location__Guard WHERE Location__Guard.locationId = "'.$locationId.'" AND Guard.guardId = Location__Guard.guardId';
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_array($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}
	function getLogCategories() {
		$query = "SELECT * FROM LogCategories ORDER BY categoryIndex ASC";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}
	function updateLocationStatus($locationId, $state) {
		$query = "UPDATE Location SET locationAlertsEnabled = ".$state." WHERE locationId = '".$locationId."'";
		mysql_query($query);

		return true;
	}

	function canUserViewLocation($lId) {
		if ($_SESSION['accountLevel'] == 0) {
			$query = "SELECT * FROM Location WHERE locationId = '".$lId."' AND locationClientId = '".$_SESSION['aId']."'";
			$res = mysql_query($query);
			$n = mysql_num_rows($res);
		} else if ($_SESSION['accountLevel'] == 1) {
			$query = "SELECT * FROM Location__Guard WHERE locationId = '".$lId."' AND guardId = '".$_SESSION['aId']."'";
			$res = mysql_query($query);
			$n = mysql_num_rows($res);
		}
		return ($n > 0) ? true : false;
	}

	function getLocationsForUser() {
		if ($_SESSION['accountLevel'] == 0) {
			$query = "SELECT locationId, locationName FROM Location, User WHERE User.userId = '".$_SESSION['id']."' AND Location.locationClientId = User.accountId";
		} else {
			$query = "SELECT Location.locationId, Location.locationName FROM Location, Location__Guard WHERE Location__Guard.guardId = '".$_SESSION['aId']."' AND Location.locationId = Location__Guard.locationId";
		}
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_row($res)) {
			array_push($assoc_array, array($row[1], $row[0]));
		}
		if (count($assoc_array) == 0) {
			return '';
		} else {
			return $assoc_array;
		}
		return '';
	}
	function getLocationData($lId) {
		$query = "SELECT * FROM Location WHERE locationId = '".$lId."'";
		$res = mysql_query($query);
		$data = mysql_fetch_assoc($res);
		$query = "SELECT * FROM User WHERE User.accountId = '".$data['locationClientId']."'";
		$res2 = mysql_query($query);
		$user = mysql_fetch_assoc($res2);
		return array($data, $user);
	}



	/* MESSAGES */
	function createMessage($m) {
		if ($_SESSION['accountLevel'] == 0) {
			$query = "INSERT INTO Message(messageId, messageFromId, messageLocationId, messageContent, messageCreated) VALUES(ObjectId(), '".$_SESSION['id']."', '".$m['location']."', '".$m['message']."', NOW())";
		} else {
			$query = "INSERT INTO Message(messageId, messageToId, messageLocationId, messageContent, messageCreated) VALUES(ObjectId(), '".$_SESSION['id']."', '".$m['location']."', '".$m['message']."', NOW())";
		}
		$res = mysql_query($query);
		return true;
	}
	function getMessagesForLocation($lId) {
		$query = "SELECT Message.messageContent AS message, User.username AS username, Message.messageCreated AS sentAt FROM Message, User WHERE Message.messageLocationId = '".$lId."' AND User.userId = Message.messageFromId OR Message.messageLocationId = '".$lId."' AND User.userId = Message.messageToId ORDER BY Message.messageCreated DESC";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}
	function getJSONMessagesForLocation($lId) {
		$query = "SELECT Message.messageContent AS message, User.username AS username, Message.messageCreated AS sentAt FROM Message, User WHERE Message.messageLocationId = '".$lId."' AND User.userId = Message.messageFromId OR Message.messageLocationId = '".$lId."' AND User.userId = Message.messageToId ORDER BY Message.messageCreated DESC";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		$d = [];
		$d['user'] = $_SESSION['username'];
		$d['messages'] = $assoc_array;
		return json_encode($d);
	}

	/* LOGS */
	function createLogEntry($data) {
		//set timezone
		date_default_timezone_set('pacific/auckland');
		//get location id
		$res = mysql_query('SELECT ObjectId() AS Id');
		$id = mysql_fetch_object($res)->Id;

		$dateString = $data[1]['month'].'/'.$data[1]['day'].'/'.$data[1]['year'].' '.$data[1]['hour'].':'.$data[1]['minute'];
		$d = strtotime($dateString);
		$d = date('Y-m-d H:i', $d);
		$query = "INSERT INTO LogEntry(logEntryId, logGuardId, logLocationId, logType, logCategoryId, logComment, logTimestamp) VALUES('".$id."', '".$_SESSION['aId']."', '".$data[3]."', '".$data[2]."', '".$data[0]['t']."', '".mysql_real_escape_string($data[0]['c'])."', '".$d."')";
		$res2 = mysql_query($query);
		if ($data[2] == 1) {
			//send IR emails out
			$res2 = sendIncidentReports($data[3], $id);
		}

		return $res2;
	}
	function getLogsForLocation($lId) {
		$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, LogCategories.category FROM LogEntry, Guard, LogCategories WHERE Guard.guardId = LogEntry.logGuardId AND LogCategories.logCategoriesId = LogEntry.logCategoryId AND LogEntry.logLocationId = '".$lId."' ORDER BY LogEntry.logTimestamp DESC LIMIT 1000";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}
	function getAllLogs() {
		if ($_SESSION['accountLevel'] == 1 || $_SESSION['accountLevel'] == 2 || $_SESSION['accountLevel'] == 3) {
			$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, Location.locationName, LogCategories.category FROM LogEntry, Guard, Location, LogCategories WHERE Guard.guardId = LogEntry.logGuardId AND Location.locationId = LogEntry.logLocationId AND LogCategories.logCategoriesId = LogEntry.logCategoryId ORDER BY LogEntry.logTimestamp DESC, Location.locationName ASC LIMIT 100";
		} else {
			$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, Location.locationName, LogCategories.category FROM LogEntry, Guard, Location, LogCategories WHERE Guard.guardId = LogEntry.logGuardId AND Location.loactionClientId = '".$_SESSION['aId']."' AND Location.locationId = LogEntry.logLocationId AND LogCategories.logCategoriesId = LogEntry.logCategoryId ORDER BY LogEntry.logTimestamp DESC, Location.locationName ASC LIMIT 100";
		}

		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function getFilteredLogs($locations, $types, $categories, $guards, $from, $to) {
		$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, Location.locationName, LogCategories.category FROM LogEntry, Guard, Location, LogCategories WHERE Location.locationName IN ('".implode("','", $locations)."') AND Guard.guardName IN ('".implode("','", $guards)."') AND LogCategories.category IN ('".implode("','", $categories)."') AND LogEntry.logType IN ('".implode(',', $types)."') AND Guard.guardId = LogEntry.logGuardId AND Location.locationId = LogEntry.logLocationId AND LogCategories.logCategoriesId = LogEntry.logCategoryId AND LogEntry.logTimestamp BETWEEN '".$from."' AND '".$to."' ORDER BY LogEntry.logTimestamp DESC LIMIT 100";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function getAllLogsForUser() {
		if ($_SESSION['accountLevel'] == 0) {
			$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, LogCategories.category, Location.locationId, Location.locationName FROM LogEntry, Guard, LogCategories, Location WHERE Guard.guardId = LogEntry.logGuardId AND LogCategories.logCategoriesId = LogEntry.logCategoryId AND Location.locationClientId = '".$_SESSION['aId']."' AND LogEntry.logLocationId = Location.locationId ORDER BY LogEntry.logTimestamp DESC, Location.locationName ASC";
		} else if ($_SESSION['accountLevel'] == 1) {
			$query = "SELECT LogEntry.logType, LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, LogCategories.category, Location.locationName FROM LogEntry, Guard, LogCategories, Location__Guard, Location WHERE Location__Guard.guardId = '".$_SESSION['aId']."' AND LogCategories.logCategoriesId = LogEntry.logCategoryId AND LogEntry.logLocationId = Location__Guard.locationId AND Location.locationId = Location__Guard.locationId AND Guard.guardId = '".$_SESSION['aId']."' ORDER BY LogEntry.logTimestamp DESC";
		}
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}

	/* ALERTS */
	function getAllAlerts() {
		$query = "SELECT Alert.alertId, Alert.alertType, Alert.alertComment, Alert.alertTimestamp, Location.locationName FROM Alert, Location WHERE Location.locationId = Alert.alertLocation ORDER BY Alert.alertTimestamp DESC";

		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}

	function createLogCategory($name) {
		$query = "INSERT INTO LogCategories(logCategoriesId, category) VALUES(ObjectId(), '".$name."')";
		mysql_query($query);
		return true;
	}

	/* MAX INTERVALS */
	function getMaxIntervalsForLocation($lId) {
		$query = "SELECT * FROM MaximumLogTime WHERE logLocationId = '".$lId."' ORDER BY fromDateTimeStr ASC";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}
	function getAllMaxIntervals() {
		$query = "SELECT * FROM MaximumLogTime WHERE 1 ORDER BY fromDateTimeStr ASC";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function createMaxInterval($data) {
		$query = "INSERT INTO MaximumLogTime(maximumLogTimeId, logLocationId, fromDateTimeStr, untilDateTimeStr, maxInterval, maxIntervalUnit) VALUES(ObjectId(), '".$data['location']."', '".$data['start']."', '".$data['finish']."', '".$data['tv']."', '".$data['tu']."')";
		mysql_query($query);
		return $query;
	}

	/* ADMINS */
	function getAllAdmins() {
		$query = "SELECT User.username, User.email, User.userId FROM User WHERE User.accountType = 2";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function getAllLAdmins() {
		$query = "SELECT User.username, User.email, User.userId FROM User WHERE User.accountType = 3";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function createAdmin($data) {
		$query = "INSERT INTO User(userId, username, password, email, accountType) VALUES(ObjectId(), '".$data['adm-username']."', '".hash('sha256', $data['adm-password'])."', '".$data['adm-email']."', 2)";
		mysql_query($query);
		return true;
	}
	function createLAdmin($data) {
		$query = "INSERT INTO User(userId, username, password, email, accountType) VALUES(ObjectId(), '".$data['adm-username']."', '".hash('sha256', $data['adm-password'])."', '".$data['adm-email']."', 3)";
		mysql_query($query);
		return true;
	}
	function deleteAdmin($id) {
		$query = "DELETE FROM User WHERE userId = '".$id."'";
		mysql_query($query);
		return true;
	}

	/* IR EMAILS */
	function getAllIrEmails() {
		$query = "SELECT * FROM IncidentReportEmail WHERE 1";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function createIrEmail($data) {
		$query = "INSERT INTO IncidentReportEmail(incidentReportEmailId, IREmail) VALUES(ObjectId(), '".$data['ir-email']."')";
		mysql_query($query);
		return true;
	}
	function deleteIrEmail($id) {
		$query = "DELETE FROM IncidentReportEmail WHERE incidentReportEmailId = '".$id."'";
		mysql_query($query);
		return true;
	}

	function getAllAlertEmails() {
		$query = "SELECT * FROM AlertEmail WHERE 1";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}
		return json_encode($assoc_array);
	}
	function createAlertEmail($data) {
		$query = "INSERT INTO AlertEmail(alertEmailId, alertEmail) VALUES(ObjectId(), '".$data['alert-email']."')";
		mysql_query($query);
		return true;
	}
	function deleteAlertEmail($id) {
		$query = "DELETE FROM AlertEmail WHERE alertEmailId = '".$id."'";
		mysql_query($query);
		return true;
	}

	//DELETE
	function deleteClient($id) {
		$query = "SELECT accountId as Id FROM User WHERE userId='".$id."'";
		$aid = mysql_query($query);
		$aid = mysql_fetch_assoc($aid)['Id'];
		$query = "DELETE FROM Client WHERE clientId = '".$aid."'";
		mysql_query($query);
		$query = "DELETE FROM User WHERE userId = '".$id."'";
		mysql_query($query);
		return true;
	}
	function deleteGuard($id) {
		$query = "SELECT accountId as Id FROM User WHERE userId='".$id."'";
		$aid = mysql_query($query);
		$aid = mysql_fetch_assoc($aid)['Id'];
		$query = "DELETE FROM Guard WHERE guardId = '".$aid."'";
		mysql_query($query);
		$query = "DELETE FROM User WHERE userId = '".$id."'";
		mysql_query($query);
		return true;
	}
	function deleteLocation($id) {
		$query = "DELETE FROM Location WHERE locationId = '".$id."'";
		mysql_query($query);
		return true;
	}
	function deleteMaximumLogInt($id) {
		$query = "DELETE FROM MaximumLogTime WHERE maximumLogTimeId = '".$id."'";
		mysql_query($query);
		return true;
	}

	//SAVE
	function saveClient($data, $id) {
		//Update User
		$query = "UPDATE User SET username = '".$data['c_name']."', email = '".$data['u_email']."' WHERE userId = '".$id."'";
		mysql_query($query);
		//Update Client
		$query = "SELECT accountId as Id FROM User WHERE userId='".$id."'";
		$res = mysql_query($query);
		$aId = mysql_fetch_assoc($res)['Id'];
		$query = "UPDATE Client SET clientName = '".$data['c_name']."', contactName = '".$data['c_n_name']."', contactEmails = '".$data['c_emails']."', clientPhone = '".$data['c_num']."', clientAddress = '".$data['c_add']."', clientCity = '".$data['c_cit']."' WHERE clientId = '".$aId."'";
		mysql_query($query);
		return true;
	}
	function saveGuard($data, $id) {
		//Update User
		$query = "UPDATE User SET username = '".$data['g_name']."', email = '".$data['u_email']."' WHERE userId = '".$id."'";
		mysql_query($query);
		//Update Guard
		$query = "SELECT accountId as Id FROM User WHERE userId='".$id."'";
		$res = mysql_query($query);
		$aId = mysql_fetch_assoc($res)['Id'];
		$query = "UPDATE Guard SET guardName = '".$data['g_name']."', guardNumber = '".$data['g_num']."', guardPhone = '".$data['g_phone']."' WHERE guardId = '".$aId."'";
		mysql_query($query);
		return true;
	}
	function saveLocation($data, $id) {
		//Delete all Guard__Locations
		$query = "DELETE FROM Location__Guard WHERE locationId = '".$id."'";
		mysql_query($query);
		//Update location
		$query = "UPDATE Location SET locationName = '".$data['l_name']."', locationAddress = '".$data['l_add']."', locationCity = '".$data['l_cit']."', locationClientId = '".$data['c']."' WHERE locationId = '".$id."'";
		mysql_query($query);
		return true;
	}
	function saveLogCategoryOrder($data) {
		$i = 0;
		foreach ($data as $category) {
			$query = "UPDATE LogCategories SET categoryIndex = ".$i." WHERE logCategoriesId = '".$category."'";
			mysql_query($query);
			$i++;
		}
		return true;
	}

?>
