<div class="location-window login-form">
	<div class="alerts-filter-wrap">
		<div class="log-filter-wrap">
			<small>Filter:</small>
			<div class="switch switch-square"
				data-on-label="<i class='fui-check'></i>"
				data-off-label="<i class='fui-cross'></i>">
				<input type="checkbox" />
			</div>
		</div>
		<div class="log-controls-wrap">
			<div class="form-group">
				<select id="alert-location" class="mbl select-block select nrm" multiple="">
				<?php
					foreach ($locations as $obj) {
				?>
						<option value="<?= $obj[0] ?>"><?= $obj[0] ?></option>
				<?php	
					}
				?>
				</select>
			</div>
			<div class="form-group">
				<select id="alert-lt" class="mbl select-block select nrm" multiple="">
					<option value="log">Log Entry</option>
					<option value="ir">Incident Report</option>
				</select>
			</div>
			<div class="form-group">
				<select id="alert-cat" class="mbl select-block select nrm" multiple="">
				<?php
					foreach ($categories as $obj) {
				?>
						<option value="<?= $obj[1] ?>"><?= $obj[1] ?></option>
				<?php	
					}
				?>
				</select>
			</div>
			<div class="form-group">
				<select id="alert-guard" class="mbl select-block select nrm" multiple="">
				<?php
					foreach ($guards as $obj) {
				?>
						<option value="<?= $obj[0] ?>"><?= $obj[0] ?></option>
				<?php	
					}
				?>
				</select>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-xs-6">
						<label>From (HH:mm dd/MM/yyyy)</label>
						<input id="alert-from" class="form-control" type="text" placeholder="from dd/mm/yyyy">
						<p class="filter-error" id="a-f-error">Must be HH:mm dd/MM/yyyy</p>
						<p class="filter-error" id="a-d-error">From must be before to</p>
					</div>
					<div class="col-xs-6">
						<label>To (HH:mm dd/MM/yyyy)</label>
						<input id="alert-to" class="form-control" type="text" placeholder="to dd/mm/yyyy">
						<p class="filter-error" id="a-t-error">Must be HH:mm dd/MM/yyyy</p>
					</div>
				</div>
			</div>
			<div class="form-group">
				<button class="btn btn-lg btn-block btn-info" onclick="applyAlertFilter()">Apply Filter</button>
			</div>
	</div>
	<hr style="border-color: rgba(52,73,95,0.77); margin: 20px 10px;" />
	<div class="alerts-viewer-wrap">
		<?php
			foreach ($lg as $i) {
				if ($i['logType'] == 0) {
					$i['logType'] = 'Log Entry';
					$lc = 'log-type-title';
				} else {
					$i['logType'] = 'Incident Report';
					$lc = 'ir-type-title';
				}
		?>
			<div class="log-entry-wrapper">
				<span class="log-location-name"><?= $i['locationName'] ?></span>
				<span class="log-info-sep"> | </span>
				<span class="log-log-type <?= $lc ?>"><?= $i['logType'] ?></span>
				<span class="log-info-sep"> | </span>
				<span class="log-log-time"><?= $i['logTimestamp'] ?></span><br>
				<span class="log-guard-name"><?= $i['guardName'] ?></span>
				<span class="log-info-sep"> | </span>
				<span class="log-category"><?= $i['category'] ?></span>
				<span class="log-info-sep"> | </span>
				<span class="log-log-comment"><?= $i['logComment'] ?></span>
			</div>
		<?php
			}
		?>
	</div>
	<?php
		if ($accLvl >= 1) {
	?>
	<hr id="alert-hr" style="border-color: rgba(52,73,95,0.77); margin: 20px 10px;" />
	<div id="alert-btn" class="form-group">
		<button class="btn btn-lg btn-block btn-primary" onclick="outputExcel()">Export to Excel</button>
	</div>
	<?php
		}
	?>
</div>