<html>

	<head>

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>iGuard | admin</title>

		<!-- stylesheets -->
		<link href="css/main.css" rel="stylesheet">
		<link href="css/admin.css" rel="stylesheet">
		<link href="css/flaticon.css" rel="stylesheet">
		<link href="css/loading.css" rel="stylesheet">
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="bootstrap/css/prettify.css" rel="stylesheet">
		<link href="css/timepicker.css" rel="stylesheet">
		<!-- flat-ui -->
		<link href="flat/css/flat-ui.css" rel="stylesheet">

	</head>

	<body>

		<div id="pageWrapper">

			<div id="admin-header" class="no-sel">
				<h1>iGuard <font style="font-size: 20px; font-weight: 300; color: #34495e;">admin</font></h1>
				<a class="admin-logout-link" href="/logout">Logout</a>
			</div>

			<div id="admin-wrapper">
				<div class="side-bar-wrap" class="no-sel">
					<ul class="no-sel side-bar-list">
						<li class="side-bar-item selected" data-tab="guard"><span class="fui-lock side-bar-icon"></span><span class="side-bar-item-name">Guards</span></li>
						<?php if ($al == 2) { echo '<li class="side-bar-item" data-tab="client"><span class="fui-eye side-bar-icon"></span><span class="side-bar-item-name">Clients</span></li>'; } ?>
						<?php if ($al == 2) { echo '<li class="side-bar-item" data-tab="location"><span class="fui-location side-bar-icon"></span><span class="side-bar-item-name">Locations</span></li>'; } ?>
						<li class="side-bar-item" data-tab="log"><span class="fui-new side-bar-icon"></span><span class="side-bar-item-name">Logs</span></li>
						<?php if ($al == 2) { echo '<li class="side-bar-item" data-tab="log-cat"><span class="fui-list side-bar-icon"></span><span class="side-bar-item-name">Log Categories</span></li>'; } ?>
						<li class="side-bar-item" data-tab="alert"><span class="fui-eye side-bar-icon"></span><span class="side-bar-item-name">Alerts</span></li>
						<?php if ($al == 2) { echo '<li class="side-bar-item" data-tab="max-time"><span class="fui-time side-bar-icon"></span><span class="side-bar-item-name">Max No Log Time</span></li>'; } ?>
						<?php if ($al == 2) { echo '<li class="side-bar-item" data-tab="setting"><span class="fui-gear side-bar-icon"></span><span class="side-bar-item-name">Settings</span></li>'; } ?>
					</ul>
				</div>
				<div class="template-wrapper">
					<div class="loading-screen">
						<div class="loading-container">
						  <div class="ball"></div>
						  <div class="ball"></div>
						  <div class="ball"></div>
						  <div class="ball"></div>
						  <div class="ball"></div>
						  <div class="ball"></div>
						  <div class="ball"></div>
						</div>
					</div>

					<div class="template" data-template="guard">
						<div id="current-guard-view" class="half-left">
							<p class="admin-title">Guards</p>
							<div class="form-group">
								<button btn-object="guard" btn-action="new" class=" admin-form-action btn btn-primary btn-md btn-block slide-btn selected">new guard</button>
							</div>
							<div class="form-group" id="guard-button-wrap">
								<small id="no-guards-text" class="no-object-alert">there are no guards</small>
							</div>
						</div>
						<div id="guard-view" class="half-right">
							<p class="admin-title">Editor</p>
							<form id="guard-form">
								<div class="form-group collapsed">
									<small class="form-label">name</small>
									<input type="text" name="g_name" placeholder="guard name" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">number</small>
									<input type="text" name="g_num" placeholder="guard number" class="form-control input-lg" data-type="number">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">phone</small>
									<input type="text" name="g_phone" placeholder="guard phone" class="form-control input-lg" data-type="phone">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">email</small>
									<input type="text" name="u_email" placeholder="email" class="form-control input-lg" data-type="email">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">password</small>
									<input type="password" name="u_pass" placeholder="password" class="form-control input-lg" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group" data-control-type="new">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createGuard()">create guard</button>
								</div>
								<div class="form-group" data-control-type="edit">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="saveGuard()">save guard</button><br>
									<button class="btn btn-danger btn-lg btn-block" type="button" onclick="deleteGuard()">delete guard</button>
								</div>
							</form>
						</div>
					</div>

					<?php if ($al == 2) {
						?>
					<div class="template" data-template="client">
						<div id="current-client-view" class="half-left">
							<p class="admin-title">Clients</p>
							<div class="form-group">
								<button btn-object="client" btn-action="new" class=" admin-form-action btn btn-primary btn-md btn-block slide-btn selected">new client</button>
							</div>
							<div class="form-group" id="client-button-wrap">
								<small id="no-clients-text" class="no-object-alert">there are no clients</small>
							</div>
						</div>
						<div id="client-view" class="half-right">
							<p class="admin-title">Editor</p>
							<form id="client-form">
								<div class="form-group collapsed">
									<small class="form-label">name</small>
									<input type="text" name="c_name" placeholder="client name" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">contact name</small>
									<input type="text" name="c_n_name" placeholder="contact name" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">contact emails</small>
									<input type="text" name="c_emails" placeholder="contact emails" class="form-control input-lg" data-type="email-sep">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">phone</small>
									<input type="text" name="c_num" placeholder="phone" class="form-control input-lg" data-type="phone">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">address</small>
									<input type="text" name="c_add" placeholder="address 1" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">city</small>
									<input type="text" name="c_cit" placeholder="city" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">account email</small>
									<input type="email" name="u_email" placeholder="account email" class="form-control input-lg" data-type="email">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">password</small>
									<input type="password" name="u_pass" placeholder="password" class="form-control input-lg" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group" data-control-type="new">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createClient()">create client</button>
								</div>
								<div class="form-group" data-control-type="edit">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="saveClient()">save client</button><br>
									<button class="btn btn-danger btn-lg btn-block" type="button" onclick="deleteClient()">delete client</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>

					<?php if ($al == 2) {
						?>
					<div class="template" data-template="location">
						<div id="current-location-view" class="half-left">
							<p class="admin-title">Locations</p>
							<div class="form-group">
								<button btn-object="location" btn-action="new" class=" admin-form-action btn btn-primary btn-md btn-block slide-btn selected">new location</button>
							</div>
							<div class="form-group" id="location-button-wrap">
								<small id="no-locations-text" class="no-object-alert">there are no locations</small>
							</div>
						</div>
						<div id="location-view" class="half-right">
							<p class="admin-title">Editor</p>
							<form id="location-form">
								<div class="form-group collapsed">
									<small class="form-label">name</small>
									<input a="a" type="text" name="l_name" placeholder="location name" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">address</small>
									<input a="a" type="text" name="l_add" placeholder="address 1" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">city</small>
									<input a="a" type="text" name="l_cit" placeholder="city" class="form-control input-lg" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">client</small>
									<select a="a" name="c" class="mbl select select-block">
								<?php
									foreach ($clients as $c) {
								?>
										<option value="<?= $c[1] ?>"><?= $c[0] ?></option>
								<?php
									}
								?>
									</select>
								</div>
								<div class="form-group collapsed">
									<small class="form-label">associated guards</small>
									<select name="g" class="mbl select select-block" multiple="">
										<?php
									foreach ($guards as $g) {
								?>
										<option value="<?= $g[1] ?>"><?= $g[0] ?></option>
								<?php
									}
								?>
									</select>
								</div>
								<hr class="form-sep"/>
								<div class="form-group" data-control-type="new">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createLocation()">create location</button>
								</div>
								<div class="form-group" data-control-type="edit">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="saveLocation()">save location</button><br>
									<button class="btn btn-danger btn-lg btn-block" type="button" onclick="deleteLocation()">delete location</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>

					<div class="template" data-template="log">
						<div id="log-filter-view" class="half-left">
							<p class="admin-title">Log Filter</p>
							<div class="form-group collapsed">
								<small class="form-label">location</small>
								<select name="filter-location" class="select-picker mbl select select-block" multiple="">

								</select>
								<p class="select-all" data-for="filter-location">select all</p>
							</div>
							<div class="form-group collapsed">
								<small class="form-label">log type</small>
								<select name="filter-type" class="select-picker mbl select select-block" multiple="">
									<option value="0" selected="">Log Entry</option>
									<option value="1" selected="">Incident Report</option>
								</select>
								<p class="select-all" data-for="filter-type">select all</p>
							</div>
							<div class="form-group collapsed">
								<small class="form-label">log category</small>
								<select name="filter-cat" class="select-picker mbl select select-block" multiple="">
							<?php
								foreach ($categories as $c) {
							?>
									<option value="<?= $c[0] ?>"><?= $c[1] ?></option>
							<?php
								}
							?>
								</select>
								<p class="select-all" data-for="filter-cat">select all</p>
							</div>
							<div class="form-group collapsed">
								<small class="form-label">guard</small>
								<select name="filter-g" class="select-picker mbl select select-block" multiple="">

								</select>
								<p class="select-all" data-for="filter-g">select all</p>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<small class="form-label">datetime from</small>
										<input name="filter-from" type="text" class="form-control" data-time="true">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<small class="form-label">datetime to</small>
										<input name="filter-to" type="text" class="form-control" data-time="true">
									</div>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-lg btn-block btn-info" onclick="applyFilter()">Apply Filter</button>
								<button id="export-filter" class="btn btn-lg btn-block btn-info" onclick="exportLogs()">Export to Excel</button>
							</div>
						</div>
						<div id="log-filtered-view" class="half-right">
							<div class="log-entry-wrapper">
								<span class="log-location-name">My Shed</span>
								<span class="log-info-sep"> | </span>
								<span class="log-log-type log-type-title">Log Entry</span>
								<span class="log-info-sep"> | </span>
								<span class="log-log-time">02/12/1994 3:33</span><br>
								<span class="log-guard-name">Sheldon</span>
								<span class="log-info-sep"> | </span>
								<span class="log-category">Patrol</span>
								<span class="log-info-sep"> | </span>
								<span class="log-log-comment">All Clear</span>
							</div>
						</div>
					</div>

					<?php if ($al == 2) {
						?>
					<div class="template" data-template="log-cat">
						<div id="new-cat-view" class="half-left">
							<p class="admin-title">Editor</p>
							<form name="new-log-cat">
								<div class="form-group">
									<small class="form-label">category name</small>
									<input name="cat-name" class="form-control" type="text" placeholder="category name">
								</div>
								<hr class="form-sep" />
								<div class="form-group" data-control-type="new" data-control-type="edit">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createLogCat()">create log category</button>
								</div>
							</form>
							<small style="color: #e74c3c;">Note: categories can not be edited or removed</small>
						</div>
						<div id="log-cat-view" class="half-right">
							<ul id="log-cat-list">

							</ul>
							<hr class="form-sep" />
							<div class="form-group" data-control-type="new" data-control-type="edit">
								<button class="btn btn-primary btn-lg btn-block" type="button" onclick="saveCatOrder()">save category order</button>
							</div>
						</div>
					</div>
					<?php
					}
					?>

					<div class="template" data-template="alert">
						<div id="alert-filter-view" class="half-left">
							<p class="admin-title">Alert Filter</p>
							<div class="form-group collapsed">
								<small class="form-label">location</small>
								<select name="filter-location" class="mbl select select-block" multiple="">

								</select>
								<p class="select-all" data-for="filter-location">select all</p>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<small class="form-label">datetime from</small>
										<input name="filter-from" type="text" class="form-control" data-time="true">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<small class="form-label">datetime to</small>
										<input name="filter-to" type="text" class="form-control" data-time="true">
									</div>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-lg btn-block btn-info" onclick="applyAlertFilter()">Apply Filter</button>
								<button id="export-filter" class="btn btn-lg btn-block btn-info" onclick="exportAlerts()">Export to Excel</button>
							</div>
						</div>
						<div id="alert-filtered-view" class="half-right">

						</div>
					</div>

					<?php if ($al == 2) {
						?>
					<div class="template" data-template="max-time">
						<div id="max-location-view" class="half-left">
							<p class="admin-title">Locations</p>
							<div id="max-location-wrap" class="form-group">

							</div>
						</div>
						<div id="max-form-editor" class="half-right">
							<p class="admin-title">Editor</p>
							<span class="admin-title-sep">|</span>
							<span class="location-enable-link enabled">Disable location alerts</span>
							<div id="max-int-view">

							</div>
							<form name="new-max-time">
								<div class="form-group collapsed">
									<small class="form-label">from</small>
									<div class="row">
										<div class="col-xs-6">
											<select name="w-day-from" class="mbl select select-block">
												<option value="mon" selected="">Monday</option>
												<option value="tue">Tuesday</option>
												<option value="wed">Wednesday</option>
												<option value="thu">Thursday</option>
												<option value="fri">Friday</option>
												<option value="sat">Saturday</option>
												<option value="sun">Sunday</option>
											</select>
										</div>
										<div class="col-xs-6">
											<select name="time-from" class="mbl select select-block">

											</select>
										</div>
									</div>
								</div>
								<div class="form-group collapsed">
									<small class="form-label">until</small>
									<div class="row">
										<div class="col-xs-6">
											<select name="w-day-to" class="mbl select select-block">
												<option value="mon" selected="">Monday</option>
												<option value="tue">Tuesday</option>
												<option value="wed">Wednesday</option>
												<option value="thu">Thursday</option>
												<option value="fri">Friday</option>
												<option value="sat">Saturday</option>
												<option value="sun">Sunday</option>
											</select>
										</div>
										<div class="col-xs-6">
											<select name="time-to" class="mbl select select-block">

											</select>
										</div>
									</div>
								</div>
								<div class="form-group collapsed">
									<small class="form-label">max interval</small>
									<div class="row">
										<div class="col-xs-6">
											<select name="time-val" class="mbl select select-block">
											</select>
										</div>
										<div class="col-xs-6">
											<select name="time-unit" class="mbl select select-block">
												<option value="minutes" selected="">minutes</option>
												<option value="hours">hours</option>
											</select>
										</div>
									</div>
								</div>
								<hr class="form-sep" />
								<div class="form-group" data-control-type="edit">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createMaxTime()">create max interval</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>

				  <?php if ($al == 2) {
						?>
					<div class="template" data-template="setting">
						<div class="half-left">
							<p class="admin-title">Incident Report Email Addresses</p>
							<form id="ir-email-form">
								<div class="form-group collapsed">
									<small class="form-label">email address</small>
									<input class="form-control" type="text" name="ir-email" placeholder="email" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createEmail()">create email</button>
								</div>
							</form>
							<div id="ir-email-view">

							</div>
							<hr class="form-sep" />
							<p class="admin-title">Alert Email Addresses</p>
							<form id="alert-email-form">
								<div class="form-group collapsed">
									<small class="form-label">email address</small>
									<input class="form-control" type="text" name="alert-email" placeholder="email" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createAlertEmail()">create email</button>
								</div>
							</form>
							<div id="alert-email-view">

							</div>
						</div>
						<div class="half-right">
							<p class="admin-title">Admin Accounts</p>
							<form id="admin-account-form">
								<div class="form-group collapsed">
									<small class="form-label">username</small>
									<input class="form-control" type="text" name="adm-username" placeholder="username" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">email</small>
									<input class="form-control" type="text" name="adm-email" placeholder="email" data-type="email">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">password</small>
									<input class="form-control" type="password" name="adm-password" placeholder="password" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createAdmin()">create admin account</button>
								</div>
							</form>
							<div id="adm-view">

							</div>

							<p class="admin-title">Low Admin Accounts</p>
							<form id="low-admin-account-form">
								<div class="form-group collapsed">
									<small class="form-label">username</small>
									<input class="form-control" type="text" name="adm-username" placeholder="username" data-type="var">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">email</small>
									<input class="form-control" type="text" name="adm-email" placeholder="email" data-type="email">
								</div>
								<div class="form-group collapsed">
									<small class="form-label">password</small>
									<input class="form-control" type="password" name="adm-password" placeholder="password" data-type="var">
								</div>
								<hr class="form-sep" />
								<div class="form-group">
									<button class="btn btn-primary btn-lg btn-block" type="button" onclick="createLAdmin()">create admin account</button>
								</div>
							</form>
							<div id="l-adm-view">

							</div>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>

		</div>

			<!-- scripts -->
			<script src="js/jquery.min.js" type="text/javascript"></script>
			<script src="js/jquery-ui.min.js" type="text/javascript"></script>
			<script src="js/moment.js" type="text/javascript"></script>
			<script src="js/date.js" type="text/javascript"></script>
			<script src="js/timepicker.js" type="text/javascript"></script>
			<script src="js/admin_js.js" type="text/javascript"></script>
			<script src="flat/js/jquery-ui-1.10.3.custom.min.js"></script>
	    <script src="flat/js/jquery.ui.touch-punch.min.js"></script>
	    <script src="flat/js/bootstrap.min.js"></script>
	    <script src="flat/js/bootstrap-select.js"></script>
	    <script src="flat/js/bootstrap-switch.js"></script>
	    <script src="flat/js/flatui-checkbox.js"></script>
	    <script src="flat/js/flatui-radio.js"></script>
	    <script src="flat/js/jquery.tagsinput.js"></script>
	    <script src="flat/js/jquery.placeholder.js"></script>
	    <script src="flat/js/typeahead.js"></script>
	    <script src="bootstrap/js/google-code-prettify/prettify.js"></script>
	    <script src="flat/js/application.js"></script>

	</body>

</html>
