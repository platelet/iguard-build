<?php
	if ($alert) {
?>
<div id="alert-wrap">
	<div class="alert alert-<?= $alertType ?> alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	  <strong><?= $alertStrong ?></strong> <?= $alertMessage ?>
	</div>
</div>
<?php
	}
?>

<div class="no-sel formWrap login-form">
	<h4>Login</h4>
	
	<form id="loginForm" method="post" action="/login">
		<div class="form-group">
			<input type="text" name="username" placeholder="username" class="form-control input-lg login-field" value="<?= $username ?>">
			<label class="login-field-icon fui-user" for="username"></label>
		</div>
		<div class="form-group">
			<input type="password" name="password" placeholder="password" class="form-control input-lg login-field">
			<label class="login-field-icon fui-lock" for="password"></label>
		</div>
		<div class="form-group">
			<button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
		</div>
		<a class="login-link" href="/forgot">Forgot Password</a>
	</form>
</div>