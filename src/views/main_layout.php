<?php

?>

<html>

	<head>

		<!-- meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- title -->
		<title><?= $pageTitle ?></title>

		<!-- stylesheets -->
		<link href="<?= $r ?>css/main.css" rel="stylesheet">
		<link href="<?= $r ?>css/flaticon.css" rel="stylesheet">
		<!-- bootstrap -->
		<link href="<?= $r ?>bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="<?= $r ?>bootstrap/css/prettify.css" rel="stylesheet">
		<!-- flat-ui -->
		<link href="<?= $r ?>flat/css/flat-ui.css" rel="stylesheet">

		<!-- scripts -->
		<script src="<?= $r ?>js/jquery.min.js" type="text/javascript"></script>
		<script src="<?= $r ?>js/main.js" type="text/javascript"></script>
		<script src="<?= $r ?>js/date.js" type="text/javascript"></script>
		<script src="<?= $r ?>js/autosize.js" type="text/javascript"></script>

		<script>
			$(function() {$('.<?= $pageIconClass ?>').addClass('selected');});
		</script>

	</head>

	<body>

		<div id="pageWrapper">

			<div class="no-sel" id="homeHeaderWrapper">
				<h3 class="no-sel title-wel-user"><?= $username ?></h3>
				<h1 class="no-sel">iGuard</h1>
			<?php
				if ($includeLinks) {
			?>
				<ul class="icon-list">
					<?php if ($at >= 1) { echo '<li><a href="/logs" class="title-icon flaticon-calendar-alert"></a></li>'; } ?>
					<li><a href="/locations" class="title-icon flaticon-map-location"></a></li>
					<li><a href="/account" class="title-icon flaticon-user-shadow"></a></li>
					<li><a href="/logout" class="title-icon flaticon-logout-right"></a></li>
				</ul>
			<?php
				}
			?>
			</div>

			<div id="contentWrapper">
			<?php
				echo $pageContent;
			?>
			</div>

		<?php
			if ($includeFooter) {
		?>
				<div class="footer">
					<ul class="footer-links">
						<li><a href="http://www.reconsecurity.co.nz" target="_blank">recon.co.nz</a></li>
					</ul>

					<span id="copyright-notice">&copy recon security limited 2014</span>
				</div>
		<?php
			}
		?>

		</div>

		<script src="<?= $r ?>flat/js/jquery-ui-1.10.3.custom.min.js"></script>
	    <script src="<?= $r ?>flat/js/jquery.ui.touch-punch.min.js"></script>
	    <script src="<?= $r ?>flat/js/bootstrap.min.js"></script>
	    <script src="<?= $r ?>flat/js/bootstrap-select.js"></script>
	    <script src="<?= $r ?>flat/js/bootstrap-switch.js"></script>
	    <script src="<?= $r ?>flat/js/flatui-checkbox.js"></script>
	    <script src="<?= $r ?>flat/js/flatui-radio.js"></script>
	    <script src="<?= $r ?>flat/js/jquery.tagsinput.js"></script>
	    <script src="<?= $r ?>flat/js/jquery.placeholder.js"></script>
	    <script src="<?= $r ?>flat/js/typeahead.js"></script>
	    <script src="<?= $r ?>bootstrap/js/google-code-prettify/prettify.js"></script>
	    <script src="<?= $r ?>flat/js/application.js"></script>
	    <!-- datetime picker -->
		<script src="<?= $r ?>js/timepicker.js"></script>
		<link href="<?= $r ?>css/timepicker.css" rel="stylesheet">

	</body>

</html>
