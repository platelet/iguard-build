<div class="location-window login-form" data-id="<?= $l['locationId'] ?>">
	<div class="information-holder">
		<h4><?= $l['locationName'] ?></h4>
		<span><?= $l['locationAddress'] ?>, <?= $l['locationCity'] ?></span><br>
		<a href="/account/<?= $u['userId'] ?>"><?= $u['username'] ?></a>
	</div>
	<hr class="dark-hr"/>
	<div class="tab-holder">
		<div class="no-sel tab-tool-bar">
			<p class="tab hidden">messages</p>
			<?php if ($s['accountLevel'] == 1) { echo '<span class="tab-btn fui-new selected" data-tab="0"></span>'; } ?>
			<span class="tab-btn fui-chat <?php if ($s['accountLevel'] == 0) {echo 'selected';} ?>" data-tab="1"></span>
			<span class="tab-btn fui-calendar-solid" data-tab="2"></span>
			<?php if ($s['accountLevel'] == 1) { echo '<span class="tab-btn fui-time" data-tab="3"></span>'; } ?>
		</div>
		<div class="tab-body">
			<div class="tab <?php if ($s['accountLevel'] == 1) {echo 'hidden';} ?>" data-tab="1" data-name="messages">
				<div id="new-msg-wrap" class="input-group">
					<input name="new-msg" class="form-control" type="text" placeholder="new message" autocomplete="off">
					<span class="input-group-btn">
						<button type="button" class="btn" onclick="sendMessage()">
							<span class="fui-arrow-right"></span>
						</button>
					</span>
				</div>
				<div id="msg-wrap-div">
			<?php
				if (count($mes) == 0) {
			?>
					<h3 class="message-error">There are no messages</h3>
			<?php
				}
				foreach ($mes as $m) {
					if ($m['username'] == $s['username']) { $side = 'to'; } else { $side = 'from'; }
			?>
					<div class="message-wrapper <?= $side ?>">
						<p class="msg-user"><?= $m['username'] ?></p>
						<p class="message"><?= $m['message'] ?></p>
						<p class="date" data-timestamp="<?= $m['sentAt'] ?>"></p>
					</div>
			<?php
				}
			?>
				</div>
			</div>
			<?php
			if ($s['accountLevel'] == 1) {
			?>
			<div class="tab current-tab" data-tab="0" data-name="new log">
				<div id="loading-tasks">
					<p class="message-error" data-info="0">Saving Log Entry</p>
					<p class="message-error" data-info="1">Saving Incident Report</p>
					<p class="message-error" data-info="2">Log Entry Saved</p>
					<p class="message-error" data-info="3">Incident Report Saved</p>
				</div>
				<div id="new-log-form-wrap">
				<form name="new-log">
				<?php
				if ($mo) {
					//mobile setup
				?>
				<div class="row">
						<div class="col-xs-12">
							<p class="log-dt-label">Time 24hr:</p>
						</div>
						<div class="col-xs-6">
							<select class="mbl select-block select" data-dt-part="hour">

							</select>
						</div>
						<div class="col-xs-6">
							<select class="mbl select-block select" data-dt-part="minute">

							</select>
						</div>
						<div class="col-xs-12">
							<p class="log-dt-label">Date d:m:y</p>
						</div>
						<div class="col-xs-6">
							<select class="mbl select-block select" data-dt-part="day">

							</select>
						</div>
						<div class="col-xs-6">
							<select class="mbl select-block select" data-dt-part="month">

							</select>
						</div>
						<div class="col-xs-12">
							<select class="mbl select-block select" data-dt-part="year">

							</select>
						</div>
				</div>
				<?php
				} else {
					//normal
				?>
				<div class="row">
						<div class="col-xs-1">
							<p class="log-dt-label">Time 24hr:</p>
						</div>
						<div class="col-xs-2">
							<select class="mbl select-block select" data-dt-part="hour">

							</select>
						</div>
						<div class="col-xs-2">
							<select class="mbl select-block select" data-dt-part="minute">

							</select>
						</div>
						<div class="col-xs-1">
							<p class="log-dt-label">Date d:m:y</p>
						</div>
						<div class="col-xs-2">
							<select class="mbl select-block select" data-dt-part="day">

							</select>
						</div>
						<div class="col-xs-2">
							<select class="mbl select-block select" data-dt-part="month">

							</select>
						</div>
						<div class="col-xs-2">
							<select class="mbl select-block select" data-dt-part="year">

							</select>
						</div>
				</div>
				<?php
				}
				?>
				<div class="form-group">
					<select name="t" class="mbl select-block select nrm">
				<?php
					foreach ($c as $cat) {
				?>
						<option value="<?= $cat[0] ?>"><?= $cat[1] ?></option>
				<?php
					}
				?>
					</select>
				</div>
				<div class="form-group">
					<textarea name="c" type="text" placeholder="comment" class="form-control" autocomplete="off"></textarea>
				</div>
			</form>
				<div class="form-group">
					<?php
					if ($mo) {
					?>
						<div class="row">
						<div class="col-xs-12">
							<button class="btn btn-lg btn-block btn-primary" onclick="createLog()">Create Log</button>
						</div>
						<div class="col-xs-12">
							<button class="btn btn-lg btn-block btn-danger" onclick="createIR()">Create IR</button>
						</div>
					</div>
					<?php
					} else {
					?>
						<div class="row">
						<div class="col-xs-6">
							<button class="btn btn-lg btn-block btn-primary" onclick="createLog()">Create Log</button>
						</div>
						<div class="col-xs-6">
							<button class="btn btn-lg btn-block btn-danger" onclick="createIR()">Create IR</button>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				</div>
			</div>
			<?php
			}
			?>
			<div class="tab hidden" data-tab="2" data-name="logs">
			<?php
				if ($s['accountLevel'] == 1)
				{
			?>
				<div class="log-filter-wrap">
					<a href="/logs">want to filter and download?</a>
				</div>
			<?php
				}
			?>
				<div class="log-view-wrap">
					<h3 class="message-error log-error">no logs found</h3>
				<?php
					foreach ($lg as $i) {
						if ($i['logType'] == 0) {
							$i['logType'] = 'Log Entry';
							$lc = 'log-type-title';
						} else {
							$i['logType'] = 'Incident Report';
							$lc = 'ir-type-title';
						}
				?>
					<div class="log-entry-wrapper">
						<span class="log-log-type <?= $lc ?>"><?= $i['logType'] ?></span>
						<span class="log-info-sep"> | </span>
						<span class="log-log-time"><?= $i['logTimestamp'] ?></span><br>
						<span class="log-guard-name"><?= $i['guardName'] ?></span>
						<span class="log-info-sep"> | </span>
						<span class="log-category"><?= $i['category'] ?></span>
						<span class="log-info-sep"> | </span>
						<span class="log-log-comment"><?= $i['logComment'] ?></span>
					</div>
				<?php
					}
				?>
				</div>
			</div>
			<div class="tab hidden" data-tab="3" data-name="max inactivity times">
				<div class="m-l-viewer">
				<?php
					foreach ($mi as $max) {
				?>
					<div class="m-l-wrap">
						<p class="conts"><?= str_replace('::', ' ', $max['fromDateTimeStr']) ?></p>
						<p>until</p>
						<p class="conts"><?= str_replace('::', ' ', $max['untilDateTimeStr']) ?></p>
						<p class="int"><?= $max['maxInterval'] ?> <?= $max['maxIntervalUnit'] ?></p>
					</div>
				<?php
					}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
