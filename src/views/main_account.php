<div class="account-window login-form">
	<small class="account-title">account</small>
	<h2 class="account-username"><?= $username ?></h2>
	<hr class="dark-hr" />
	<div class="form-wrap">
		<form id="account-form">
		<?php
		if ($accountType == 1) {
		?>
			<small>name/username</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="name" value="<?= $data['guardName'] ?>">
			</div>
			<small>number</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="number" value="<?= $data['guardNumber'] ?>">
			</div>
			<small>phone</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="phone" value="<?= $data['guardPhone'] ?>">
			</div>
			<small>email</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="email" value="<?= $data['email'] ?>">
			</div>
			<small>password</small>
			<div class="form-group">
				<a href="/forgot">Reset Password</a>
			</div>
			<hr class="dark-hr" />
			<div class="form-group">
				<p class="message-error">Contact recon to make account changes</p>
			</div>
		<?php	
		} else {
		?>
			<small>name/username</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="name" value="<?= $data['clientName'] ?>">
			</div>
			<small>phone</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="phone" value="<?= $data['clientPhone'] ?>">
			</div>
			<small>address</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="address 1" value="<?= $data['clientAddress'] ?>">
			</div>
			<small>city</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="city" value="<?= $data['clientCity'] ?>">
			</div>
			<small>email</small>
			<div class="form-group">
				<input class="form-control flat" type="text" placeholder="email" value="<?= $data['email'] ?>">
			</div>
			<small>password</small>
			<div class="form-group">
				<a href="/forgot">reset password</a>
			</div>
			<hr class="dark-hr" />
			<div class="form-group">
				<p class="message-error">Contact recon to make account changes</p>
			</div>
		<?php
		}
		?>
		</form>
	</div>
</div>	