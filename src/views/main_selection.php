<div class="no-sel formWrap location-selector login-form">
	<h4>Select a Location</h4>
	<br>
<?php
	if ($locations == '') {
?>
		<h5><font color="#e74c3c">no locations</font></h5>
<?php
	} else {
		foreach ($locations as $loc) {
	?>
			<a class="a-no-flat" href="/locations/<?= $loc[1] ?>"><div class="form-group location-selector-wrap">
				<h5 class="location-selector-name"><?= $loc[0] ?></h5>
			</div></a>
	<?php
		}
	}
?>
</div>