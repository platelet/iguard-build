<?php
	if ($alert) {
?>
<div id="alert-wrap">
	<div class="alert alert-<?= $alertType ?> alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	  <strong><?= $alertStrong ?></strong> <?= $alertMessage ?>
	</div>
</div>
<?php
	}
?>

<div class="no-sel formWrap login-form">
	<h4>Password Reset</h4>
	
	<form id="loginForm" method="post">
		<div class="form-group">
			<input type="password" name="p_a" placeholder="password" class="form-control input-lg login-field" value="">
			<label class="login-field-icon fui-lock" for="username"></label>
		</div>
		<div class="form-group">
			<input type="password" name="p_b" placeholder="confirm" class="form-control input-lg login-field" value="">
			<label class="login-field-icon fui-lock" for="username"></label>
		</div>
		<div class="form-group">
			<button class="btn btn-primary btn-lg btn-block" type="submit">Reset Password</button>
		</div>
		<a class="login-link" href="/">Return to login</a>
	</form>
</div>