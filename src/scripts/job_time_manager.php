<?php

	require '../Mail/PHPMailerAutoload.php';
	require('mail_helper.php');

	// define('DB_USER', 'root');
	// define('DB_PASS', 'root');
	// define('DB_NAME', 'iGuard');
	// define('DB_HOST', 'localhost');

	/* MYSQL DATABASE CONNECTION */
	//daabase creditails
	define('DB_USER',getenv('DATABASE_USER'));
	define('DB_PASS',getenv('DATABASE_PASSWORD'));
	define('DB_NAME',getenv('DATABASE_NAME'));

	//connect to MySQL
	$r = mysql_connect('mysql:3306', DB_USER, DB_PASS);
	$r2 = mysql_select_db(DB_NAME);

	//set current timezone
	date_default_timezone_set('pacific/auckland');

	function ppp($j) {
		print_r('<pre>');
		print_r($j);
		print_r('</pre><br />');
	}

	// Returns all locations
	//
	// @param (none)
	// @return (array)
	function getLocations() {
		$query = "SELECT * FROM Location WHERE 1";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			$row['maxTimes'] = getMaxTimesForLocation($row['locationId']);
			$row['lastLog'] = lastLogForLocation($row['locationId']);
			$row['lastAlert'] = lastAlertForLocation($row['locationId']);
			$row['lastGuardAccess'] = lastGuardAccessForLocation($row['locationId']);
			array_push($assoc_array, $row);
		}
		return $assoc_array;
	}

	// Returns all maxTimes for location
	//
	// @param (locationId string)
	// @return (array)
	function getMaxTimesForLocation($locationId) {
		$query = "SELECT * FROM MaximumLogTime WHERE logLocationId = '".$locationId."'";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row);
		}

		return $assoc_array;
	}

	// Makes sure time is valid
	//
	// @param (time array)
	// @return (boolean)
	function isMaxTimeActive($time) {
		if (checkDateTime(explode('::', $time['fromDateTimeStr']), explode('::', $time['untilDateTimeStr'])))
			return true;
		return false;
	}

	// Get the last log for a location as assoc array
	//
	// @param (locationId string)
	// @return (array)
	function lastLogForLocation($locationId) {
		$query = "SELECT * FROM LogEntry WHERE logLocationId = '".$locationId."' ORDER BY logTimestamp DESC LIMIT 1";
		$res = mysql_query($query);
		$log = mysql_fetch_assoc($res);

		return (!empty($log)) ? $log : false;
	}

	// Get the last alert for a location as assoc array
	//
	// @param (locationId string)
	// @return (array)
	function lastAlertForLocation($locationId) {
		$query = "SELECT * FROM Alert WHERE alertLocation = '".$locationId."' ORDER BY alertTimestamp DESC LIMIT 1";
		$res = mysql_query($query);
		$alert = mysql_fetch_assoc($res);

		return (!empty($alert)) ? $alert : false;
	}

	// Get the last guard access for a location as assoc array
	//
	// @param (locationId string)
	// @return (array)
	function lastGuardAccessForLocation($locationId) {
		$query = "SELECT Guard.guardName FROM Guard, LastLocationAccess WHERE LastLocationAccess.locationId = '".$locationId."' AND Guard.guardId = LastLocationAccess.guardId";
		$res = mysql_query($query);
		$guard = mysql_fetch_assoc($res);

		return (!empty($guard)) ? $guard : false;
	}

	// Figure out if no log or alert to cover timeout
	//
	// @param (timeout int)
	// @param (lastLog array)
	// @param (lastAlert array)
	// @return (array)
	function doesLocationNeedAlert($timeout, $lastLogTimestamp, $lastAlertTimestamp) {
		$now = strtotime(date('d-m-Y H:i:s'));
		if ($lastLogTimestamp) {
			$lastLogTime = strtotime($lastLogTimestamp);
			ppp('Last log was ' . floor(($now - $lastLogTime)/60) . ' minutes ago');
			if (floor(($now - $lastLogTime)/60) > $timeout) {
				$needsAlert = true;
			} else {
				$needsAlert = false;
			}
		} else {
			$needsAlert = true;
		}
		if ($lastAlertTimestamp) {
			$lastAlertTime = strtotime($lastAlertTimestamp);
			ppp('Last alert was ' . floor(($now - $lastAlertTime)/60) . ' minutes ago');
			if (floor(($now - $lastAlertTime)/60) > $timeout) {
				return $needsAlert;
			} else {
				return false;
			}
		} else {
			return $needsAlert;
		}
	}

	function timeSinceLastLog($lastLog) {
		$now = strtotime(date('d-m-Y H:i:s'));
		if ($lastLog) {
			$lastLogTime = strtotime($lastLog['logTimestamp']);
			return floor(($now - $lastLogTime) / 60);
		} else {
			return 'location creation';
		}
	}

	function range_array($a, $b) {
		$c = [];
		if (is_int($a) && is_int($b)) {
			if ($a == $b) {
				array_push($c, $a);
			} else if ($a < 7 && $b < 7 && $b < $a) {
				for ($i = $b; $i != ($a+1); $i++) {
					array_push($c, $i);
				}
			} else {
				for ($i = $a; $i != ($b+1); $i++) {
					array_push($c, $i);
				}
			}
		}
		return $c;
	}

	/* VALID LOGIC HELPER */
	function checkDateTime($from_, $until_) {
		$fit = false;
		$now = explode('-', date('D-H:i'));
		$dayNow = strtolower($now[0]);
		$timeNow = explode(':', $now[1]);

		$days = array('mon','tue','wed','thu','fri','sat','sun');

		$today_index = array_search($dayNow, $days);
		$from_index = array_search($from_[0], $days);
		$until_index = array_search($until_[0], $days);

		if ($today_index !== false && $from_index !== false && $until_index !== false) {
			$range = range_array($from_index, $until_index);
			if (is_int(array_search($today_index, $range))) {
				//today is in the range
				if ($today_index == $from_index) {
					$s_time = explode(':', $from_[1]);
					$s_time_unix = mktime($s_time[0], $s_time[1]);
					$current_unix = new DateTime();
					$current_unix = $current_unix->getTimestamp();
					if ($current_unix > $s_time_unix) {
						$fit = true;
					}
				} else if ($today_index == $until_index) {
					$f_time = explode(':', $until_[1]);
					$f_time_unix = mktime($f_time[0], $f_time[1]);
					$current_unix = new DateTime();
					$current_unix = $current_unix->getTimestamp();
					if ($current_unix < $f_time_unix) {
						$fit = true;
					}
				}
			}
		}

		return $fit;
	}

	/* SEE IF LAST LOG WAS TOO LONG AGO */
	function wasLogToLong($location, $log, $time) {
		ppp('wasToLong --');
		ppp(var_dump($location, false));
		ppp(var_dump($log, false));
		ppp(var_dump($time, false));
		$logTimestamp = $log['logTimestamp'];
		$now = strtotime(date('d-m-Y H:i:s'));
		$then = strtotime($logTimestamp);

		$minutesPast = floor(($now-$then)/60);
		$minutesAloud = intval($time['maxInterval']);

		if ($time['maxIntervalUnit'] == 'hours') {
			$minutesAloud *= 60;
		}
		if ($minutesPast > $minutesAloud) {
			return array($minutesPast, $minutesAloud, 'minutes');
		} else {
			return false;
		}
	}

	/* GET GUARDS FOR LOCATION */
	function guardEmailsForLocation($locationId) {
		$query = "SELECT User.email FROM User, Location__Guard WHERE Location__Guard.locationId = '".$locationId."' AND User.accountId = Location__Guard.guardId;";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row['email']);
		}
		return $assoc_array;
	}
	/* CREATE ALERT OBJECT */
	function logAlert($locationId, $timestamp, $comment) {
		$query = "INSERT INTO Alert(alertId, alertType, alertComment, alertLocation, alertTimestamp) VALUES(ObjectId(), 0, '".$comment."', '".$locationId."', '".$timestamp."')";
		mysql_query($query);

		return true;
	}
	/* GET ALERT EMAILS */
	function alertEmails() {
		$query = "SELECT alertEmail FROM AlertEmail WHERE 1";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row['alertEmail']);
		}
		return $assoc_array;
	}

	/* RUN TIME */
	$logLocations = getLocations();
	$activeTimesFound = false;
	$alerts = [];

	$logContent = '';
	$logDateTime = date('d_m_H_i');
	$logContent .= "Current Datetime: " . date('D-H:i') . "\n";

	foreach ($logLocations as $location) {
		//ensure location alerts are active
		if ($location['locationAlertsEnabled'] === '1' && !empty($location['maxTimes'])) {

			ppp($location['locationName'].'('.$location['locationId'].') -- Start --');
			$logContent .= "\n\n --------------- Location '".$location['locationName']."' Start --------------- \n";

			foreach ($location['maxTimes'] as $maxTime) {
				if (isMaxTimeActive($maxTime)) {
					$activeTimesFound = true;

					ppp($location['locationName'].'('.$location['locationId'].') -- Active Time --');
					$logContent .= 'Location: ' . $location['locationName'] . ' has active max time' . "\n";

					$timeout = intval($maxTime['maxInterval']);
					if ($maxTime['maxIntervalUnit'] == 'hours')
						$timeout *= 60;
					ppp($location['locationName'].'('.$location['locationId'].') -- Log or Alert Needed in last '.$timeout.' minutes --');
					$logContent .= 'Location needed log or alert in last ' . $timeout . ' minutes' . "\n";
					$logContent .= 'Last log: ' . $location['lastLog']['logTimestamp'] . "\n";
					$logContent .= 'Last alert: ' . $location['lastAlert']['alertTimestamp'] . "\n";
					$needsAlert = doesLocationNeedAlert($timeout, $location['lastLog']['logTimestamp'], $location['lastAlert']['alertTimestamp']);
					if ($needsAlert) {
						ppp("Sending Alert \n");
						$logContent .= 'Alert being sent for location' . "\n";
						$alerts[] = array($location, $maxTime, timeSinceLastLog($location['lastLog']));
					} else {
						$logContent .= 'No alert sent for location' . "\n";
					}
				}
			}
		} else {
			ppp($location['locationName'].'('.$location['locationId'].') -- Ignoring --');
			$logContent .= "\n\n --------------- Location '".$location['locationName']."' Ignored --------------- \n";
			continue;
		}
		ppp($location['locationName'].'('.$location['locationId'].') -- Finished --');
		$logContent .= "\n\n --------------- Location '".$location['locationName']."' Finish --------------- \n";
	}

	//save the log file
	$fileName = '/opt/app-root/src/logs/log_' . $logDateTime . '.log';
	file_put_contents($fileName, $logContent);

	foreach ($alerts as $alert) {
		$irs = alertEmails();

		logAlert($alert[0]['locationId'], date('Y-m-d H:i:s'), 'A log should have been placed by '.$alert[0]['lastGuardAccess']['guardName'].' '.strval($alert[1]['maxInterval']).' '.$alert[1]['maxIntervalUnit'].' ago');

		$emails = array();
		foreach ($irs as $ir) {
			array_push($emails, $ir);
		}

		$b = '<div class="content no-sel"><h3>Location Inactivity Alert</h3><br><p>There has been no log at '.$alert[0]['locationName'].' for '.strval($alert[2]).' minutes</p><p>A log should have been placed by '.$alert[0]['lastGuardAccess']['guardName'].' in the last '.strval($alert[1]['maxInterval']).' '.$alert[1]['maxIntervalUnit'].'</p></div>';

		echo sendEmail('Inactivity Alert', $emails, '', $b);
	}

?>
