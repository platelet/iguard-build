<?php

	/* GLOBAL EMAIL PARTS */
	$html_header = '<html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><style>body{background:#2c3e50;color:#ecf0f1;font-family:Helvetica,sans-serif;overflow:hidden}#header{position:absolute;text-align:center;color:#7f8c8d;background:#ecf0f1;top:0;left:0;right:0;box-shadow:0 0 25px 10px rgba(48,48,48,.77);z-index:9999999}#header>h1{font-size:30px;margin:20px 0}.no-sel{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default}div.content{position:relative;margin:125px 40px 40px;background:#edeff1;padding:20px;border-radius:6px;color:#34495e;text-align:center}a{text-decoration:none;color:#16a085;-webkit-transition:all .25s linear;-moz-transition:all .25s linear;-o-transition:all .25s linear;transition:all .25s linear}a:hover{color:#1abc9c;text-decoration:underline}p.email-notification{text-align:center;margin:0;font-size:15px;font-weight:300;opacity:.5}.ir-details{background:#34495e;border-radius:6px;margin:30px 15px 0;text-align:left;padding:20px;color:#ecf0f1}</style></head><body class="no-sel"><div id="header" class="no-sel"><h1>iGuard</h1></div>';
	$html_ir_header = '<html><head><style>body{color:#2c3e50;font-family:Helvetica,sans-serif;overflow:hidden;margin: 0px;padding: 0px;}img {margin: 0px; padding: 0px;}div.content{position:relative;margin:0;padding:20px;text-align:left}p.email-notification{text-align:left;margin:20px;font-size:15px;font-weight:300;opacity:.8}table tr td{padding: 5px 5px 5px 0px}</style></head><body>';
	$html_footer = '<p class="email-notification">This is an automated email, please do not reply</p></body></html>';


	/* SET UP MAIL */

	function sendEmail($sub, $emails, $name, $body, $header) {

		global $html_footer;

		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'messenger@iguard.kiwi';
		$mail->Password = "iguardadmin487";
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;

		$mail->From = 'messenger@iguard.kiwi';
		$mail->FromName = 'iGuard Messenger';
		foreach ($emails as $email) {
			$mail->addAddress($email);
		}
		$mail->isHTML(true);
		$mail->Subject = $sub;
		$mail->Body = $header . $body . $html_footer;

		if(!$mail->send()) {
		    return $mail->ErrorInfo;
		} else {
			return 'your message was sent';
		}
	}

	/* END */

	/* EMAILS */

	/* GET IR EMAILS */
	function IR_Emails() {
		$query = "SELECT IREmail FROM IncidentReportEmail WHERE 1";
		$res = mysql_query($query);
		$assoc_array = [];
		while ($row = mysql_fetch_assoc($res)) {
			array_push($assoc_array, $row['IREmail']);
		}
		return $assoc_array;
	}

	//forgot token sender
	function sendResetPasswordFor($user) {
		global $html_header;
		//get reset token
		$query = "SELECT ObjectId() AS Id";
		$res = mysql_query($query);
		$token = mysql_fetch_assoc($res)['Id'];
		//create reset token row
		$query = "INSERT INTO User__ResetKey(user__resetKeyId, userId, resetToken) VALUES(ObjectId(), '".$user['userId']."', '".$token."')";
		mysql_query($query);
		//send email

		$b = '<div class="content no-sel"><h3>Password Reset Notification</h3><br><p>Please follow this <a href="http://iguard.kiwi/reset/'.$token.'">link</a> to reset your password</p><p>If you did not request a password reset please ignore this email</p></div>';

		return sendEmail('Password Reset Link', array($user['email']), $user['username'], $b, $html_header);
	}

	//ir email
	function sendIncidentReports($locationId, $id) {
		global $html_ir_header;
		//get the clients email
		$query = "SELECT User.email, User.username, Client.contactName, Client.contactEmails FROM User, Client, Location WHERE Location.locationId = '".$locationId."' AND Client.clientId = Location.locationClientId AND User.accountId = Client.clientId";
		$res = mysql_query($query);
		$user = mysql_fetch_assoc($res);
		$query = "SELECT LogEntry.logTimestamp, LogEntry.logComment, Guard.guardName, Location.locationName, LogCategories.category FROM LogEntry, Guard, Location, LogCategories WHERE LogEntry.logEntryId = '".$id."' AND Location.locationId = LogEntry.logLocationId AND Guard.guardId = LogEntry.logGuardId AND LogCategories.logCategoriesId = LogEntry.logCategoryId";
		$res2 = mysql_query($query);
		$ir = mysql_fetch_assoc($res2);
		$ir['logComment'] = utf8_decode($ir["logComment"]);

		$b = '<img src="https://s3-ap-southeast-2.amazonaws.com/iguard/recon-security-logo.jpg" width="50%">
					<div class="content no-sel"><h3>Incident Report</h3><br>
					<p><b>'.$ir['guardName'].' has logged an incident report at '.$ir['locationName'].'</b></p>
					<table class="ir-details">
  				<tr><td>Guard</td><td><font color="#7f8c8d">'.$ir['guardName'].'</font></td></tr>
        	<tr><td>Location Name</td><td><font color="#7f8c8d">'.$ir['locationName'].'</font></td></tr>
        	<tr><td>Category</td><td><font color="#7f8c8d">'.$ir['category'].'</font></td></tr>
        	<tr><td>Timestamp</td><td><font color="#7f8c8d">'.$ir['logTimestamp'].'</font></td></tr>
        	<tr><td>Comment</td><td><font color="#7f8c8d">'.$ir['logComment'].'</font></td></tr>
      		</table></div>';

		$ir = IR_Emails();

		$emails = array($user['email']);
		$contactEmails = explode('; ', $user['contactEmails']);
		foreach ($contactEmails as $ue) {
			array_push($emails, $ue);
		}
		foreach ($ir as $ie) {
			array_push($emails, $ie);
		}

		return sendEmail('Incident Report', $emails, $user['username'], $b, $html_ir_header);
	}

?>
