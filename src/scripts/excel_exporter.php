<?php

	function generateLogExcel($data, $timestamp) {
		//open file
		$fId = strval(rand()*4437286); //randon file seed
		$fileName = 'excelfiles/'.$fId.'.csv'; //file name
		$file = fopen($fileName, 'wb'); //open the file
		//write initial data
		fwrite($file, 'iGuard Logs,,,,,' . "\n");
		fwrite($file, ',,,,,' . "\n");
		fwrite($file, 'GENERATED,'.$timestamp.',,,,' . "\n");
		fwrite($file, ',,,,,' . "\n");
		fwrite($file, 'Location,Timestamp,Guard,Category,Log Type,Comment' . "\n");
		//write rows
		foreach ($data as $d) {
			fwrite($file, getLogRowFromArray($d) . "\n");
		}
		fwrite($file, "\n");
		fclose($file);
		return $fId;
	}
	function generateAlertExcel($data, $timestamp) {
		//open file
		$fId = strval(rand()*4437286); //random file seed
		$fileName = 'excelfiles/'.$fId.'.csv'; //file name
		$file = fopen($fileName, 'wb'); //open the file
		//write initial data
		fwrite($file, 'iGuard Alerts,,,,,' . "\n");
		fwrite($file, ',,,,,' . "\n");
		fwrite($file, 'GENERATED,'.$timestamp.',,,,' . "\n");
		fwrite($file, ',,,,,' . "\n");
		fwrite($file, 'Location,Timestamp,Comment' . "\n");
		//write rows
		foreach ($data as $d) {
			fwrite($file, getAlertRowFromArray($d) . "\n");
		}
		fwrite($file, "\n");
		fclose($file);
		return $fId;
	}

	function getLogRowFromArray($array) {
		if (isset($array['timestamp'])) {
			$t = $array['timestamp'];
		} else {
			$t = $array['logTimestamp'];
		}
		return '"'.$array['locationName'].'","'.$t.'","'.$array['guardName'].'","'.$array['category'].'","'.$array['logType'].'","'.$array['logComment'].'"';
	}
	function getAlertRowFromArray($array) {
		return '"'.$array['locationName'].'","'.$array['alertTimestamp'].'","'.$array['alertComment'].'"';
	}

	function downloadCSVFile($name) {
		$file = 'excelfiles/'.$name.'.csv';
		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/force-download');
		    header('Content-Type: application/octet-stream');
		    header('Content-Type: application/download');
		    header('Content-Disposition: attachment; filename='.basename($file));
		    header('Expires: 0');
		    header("Pragma: public");
		    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		    header('Content-Length: ' . filesize($file));
		    header("Accept-Ranges: bytes");
		    readfile($file);
		    exit;
		} else {
			print_r("file not found");
		}
	}

?>
