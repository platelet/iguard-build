<?php
	
	//layout render function - always called last
	function renderLayout($title, $selectedIcon, $includeLinks, $includeFooter, $r) {
		if (isset($_SESSION['username'])) {
			$u = 'You are logged in as ' . $_SESSION['username'];
		} else {
			$u = '';
		}
		
		if (isset($_SESSION['accountLevel'])) {
			$at = $_SESSION['accountLevel'];
		} else {
			$at = -1;
		}
		
		Flight::render('main_layout', array('pageTitle' => $title, 'pageIconClass' => $selectedIcon, 'includeLinks' => $includeLinks, 'includeFooter' => false, 'r' => $r, 'username' => $u, 'at' => $at));
	}
	
	//page renders - called before layout
	
	function renderPage($file, $vars, $htmlName) {
		Flight::render($file, $vars, $htmlName);
	}
	
?>