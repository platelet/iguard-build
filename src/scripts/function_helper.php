<?php
	
	function postArrayToAssocArray($a) {
		$newArray = [];
		foreach ($a as $obj) {
		    $newArray[ $obj['name'] ] = $obj['value'];
		}
		return $newArray;
	}
	
?>